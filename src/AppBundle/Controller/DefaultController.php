<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\AddNewForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(AddNewForm::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $category = new Category();
            $category->translate('ru')->setName($data['ruPhrase']);


            $em = $this->getDoctrine()->getManager();
            $em->persist($category);

            $category->mergeNewTranslations();
            $em->flush();
        }

        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        return $this->render(':default:index.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/{id}", requirements={"id": "\d+"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(int $id) {
        $category = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->find($id);

        return $this->render('default/details.html.twig', array(
            'Category' => $category
        ));
    }
}
