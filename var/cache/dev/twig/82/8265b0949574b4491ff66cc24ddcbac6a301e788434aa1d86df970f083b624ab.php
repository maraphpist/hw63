<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_d1c001d7854d4dc42289f0d0554a79f785bb8597d75766d5187c38da7138059c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3d89b13000a2115e2038e8f3e20ec35765e6611616d1cbe62bfb3955676f883 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3d89b13000a2115e2038e8f3e20ec35765e6611616d1cbe62bfb3955676f883->enter($__internal_b3d89b13000a2115e2038e8f3e20ec35765e6611616d1cbe62bfb3955676f883_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_8a042799449a195ddfde497b1128b6333ef4a5a812fc0b71eac960498b87ac85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a042799449a195ddfde497b1128b6333ef4a5a812fc0b71eac960498b87ac85->enter($__internal_8a042799449a195ddfde497b1128b6333ef4a5a812fc0b71eac960498b87ac85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_b3d89b13000a2115e2038e8f3e20ec35765e6611616d1cbe62bfb3955676f883->leave($__internal_b3d89b13000a2115e2038e8f3e20ec35765e6611616d1cbe62bfb3955676f883_prof);

        
        $__internal_8a042799449a195ddfde497b1128b6333ef4a5a812fc0b71eac960498b87ac85->leave($__internal_8a042799449a195ddfde497b1128b6333ef4a5a812fc0b71eac960498b87ac85_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
