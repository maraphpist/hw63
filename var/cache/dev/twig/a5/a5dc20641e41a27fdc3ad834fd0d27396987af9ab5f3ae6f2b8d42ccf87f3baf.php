<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_73df4408d4afd78de1a8195505139769de9e3d178bbd706eaf09d5975e7b1cfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46974b888be895945e9ead46d744986f51bfc5098e96bd6639049c7b3796012e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46974b888be895945e9ead46d744986f51bfc5098e96bd6639049c7b3796012e->enter($__internal_46974b888be895945e9ead46d744986f51bfc5098e96bd6639049c7b3796012e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_b27b78503b353d364683175fa78319845d16e5dde80855fc88bf5001736bb191 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b27b78503b353d364683175fa78319845d16e5dde80855fc88bf5001736bb191->enter($__internal_b27b78503b353d364683175fa78319845d16e5dde80855fc88bf5001736bb191_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_46974b888be895945e9ead46d744986f51bfc5098e96bd6639049c7b3796012e->leave($__internal_46974b888be895945e9ead46d744986f51bfc5098e96bd6639049c7b3796012e_prof);

        
        $__internal_b27b78503b353d364683175fa78319845d16e5dde80855fc88bf5001736bb191->leave($__internal_b27b78503b353d364683175fa78319845d16e5dde80855fc88bf5001736bb191_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
