<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_ff745af4c9785908df5f50f3220a611a7a8150c8acb4b2d4fa58002236a15742 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e8f8510d603a07a878a6125401e4056eff67cdda4218adb10f564957a3efa3d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e8f8510d603a07a878a6125401e4056eff67cdda4218adb10f564957a3efa3d9->enter($__internal_e8f8510d603a07a878a6125401e4056eff67cdda4218adb10f564957a3efa3d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_d082085e618452063db2dcf580abcf361943f9c01e457128b6bf093abf8d8c6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d082085e618452063db2dcf580abcf361943f9c01e457128b6bf093abf8d8c6f->enter($__internal_d082085e618452063db2dcf580abcf361943f9c01e457128b6bf093abf8d8c6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e8f8510d603a07a878a6125401e4056eff67cdda4218adb10f564957a3efa3d9->leave($__internal_e8f8510d603a07a878a6125401e4056eff67cdda4218adb10f564957a3efa3d9_prof);

        
        $__internal_d082085e618452063db2dcf580abcf361943f9c01e457128b6bf093abf8d8c6f->leave($__internal_d082085e618452063db2dcf580abcf361943f9c01e457128b6bf093abf8d8c6f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f4941b093f49ac07176dae065bd8cf2c068c8c939885c0c2e329088fce482c1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4941b093f49ac07176dae065bd8cf2c068c8c939885c0c2e329088fce482c1a->enter($__internal_f4941b093f49ac07176dae065bd8cf2c068c8c939885c0c2e329088fce482c1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_cddf977c8105111f4cf25d3ed56cafa44ef3c8ec413be57c3161ae3c31c74c54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cddf977c8105111f4cf25d3ed56cafa44ef3c8ec413be57c3161ae3c31c74c54->enter($__internal_cddf977c8105111f4cf25d3ed56cafa44ef3c8ec413be57c3161ae3c31c74c54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_cddf977c8105111f4cf25d3ed56cafa44ef3c8ec413be57c3161ae3c31c74c54->leave($__internal_cddf977c8105111f4cf25d3ed56cafa44ef3c8ec413be57c3161ae3c31c74c54_prof);

        
        $__internal_f4941b093f49ac07176dae065bd8cf2c068c8c939885c0c2e329088fce482c1a->leave($__internal_f4941b093f49ac07176dae065bd8cf2c068c8c939885c0c2e329088fce482c1a_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_3e98d053599abf3f0e79403338e7558a972192caa8ec93e7191c5df4461c9d9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e98d053599abf3f0e79403338e7558a972192caa8ec93e7191c5df4461c9d9a->enter($__internal_3e98d053599abf3f0e79403338e7558a972192caa8ec93e7191c5df4461c9d9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_17edf618ef16063a325e8ffcc2d062b24542904cf95d5b03b388ddd605d15b5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17edf618ef16063a325e8ffcc2d062b24542904cf95d5b03b388ddd605d15b5f->enter($__internal_17edf618ef16063a325e8ffcc2d062b24542904cf95d5b03b388ddd605d15b5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_17edf618ef16063a325e8ffcc2d062b24542904cf95d5b03b388ddd605d15b5f->leave($__internal_17edf618ef16063a325e8ffcc2d062b24542904cf95d5b03b388ddd605d15b5f_prof);

        
        $__internal_3e98d053599abf3f0e79403338e7558a972192caa8ec93e7191c5df4461c9d9a->leave($__internal_3e98d053599abf3f0e79403338e7558a972192caa8ec93e7191c5df4461c9d9a_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_1c3ee98c0ffb870fd17fac5a8a1429f98fb0cdc4f207cfbce05638b234e58f30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c3ee98c0ffb870fd17fac5a8a1429f98fb0cdc4f207cfbce05638b234e58f30->enter($__internal_1c3ee98c0ffb870fd17fac5a8a1429f98fb0cdc4f207cfbce05638b234e58f30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_082ab1aa5066b190c00fdfb8a1753c01465385d0da5f3905c8858bacc8e9af46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_082ab1aa5066b190c00fdfb8a1753c01465385d0da5f3905c8858bacc8e9af46->enter($__internal_082ab1aa5066b190c00fdfb8a1753c01465385d0da5f3905c8858bacc8e9af46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_082ab1aa5066b190c00fdfb8a1753c01465385d0da5f3905c8858bacc8e9af46->leave($__internal_082ab1aa5066b190c00fdfb8a1753c01465385d0da5f3905c8858bacc8e9af46_prof);

        
        $__internal_1c3ee98c0ffb870fd17fac5a8a1429f98fb0cdc4f207cfbce05638b234e58f30->leave($__internal_1c3ee98c0ffb870fd17fac5a8a1429f98fb0cdc4f207cfbce05638b234e58f30_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
