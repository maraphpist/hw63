<?php

/* details.html.twig */
class __TwigTemplate_3b08862ed1521ff3c0a89232fb4bada310088d02c2da42620d4380180e7e98ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::main.html.twig", "details.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::main.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebacb44d1e67915ed41397709dba808d0eaff71b643928abcaf3d733fcf93137 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebacb44d1e67915ed41397709dba808d0eaff71b643928abcaf3d733fcf93137->enter($__internal_ebacb44d1e67915ed41397709dba808d0eaff71b643928abcaf3d733fcf93137_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "details.html.twig"));

        $__internal_f2d2e5e4709e316fd967a85bcbca692c449b5be486fabd7de3b8de4c38ff5aa1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2d2e5e4709e316fd967a85bcbca692c449b5be486fabd7de3b8de4c38ff5aa1->enter($__internal_f2d2e5e4709e316fd967a85bcbca692c449b5be486fabd7de3b8de4c38ff5aa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "details.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ebacb44d1e67915ed41397709dba808d0eaff71b643928abcaf3d733fcf93137->leave($__internal_ebacb44d1e67915ed41397709dba808d0eaff71b643928abcaf3d733fcf93137_prof);

        
        $__internal_f2d2e5e4709e316fd967a85bcbca692c449b5be486fabd7de3b8de4c38ff5aa1->leave($__internal_f2d2e5e4709e316fd967a85bcbca692c449b5be486fabd7de3b8de4c38ff5aa1_prof);

    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        $__internal_da54eefe4dbb70e560e1cf60cda249b2b46509a0e7fd5ef103f3a14070e0f1d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da54eefe4dbb70e560e1cf60cda249b2b46509a0e7fd5ef103f3a14070e0f1d3->enter($__internal_da54eefe4dbb70e560e1cf60cda249b2b46509a0e7fd5ef103f3a14070e0f1d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_6d11b5e323a83f746cf46c968c40c159f6e539d292b01105b3a5ab3e7ac04782 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d11b5e323a83f746cf46c968c40c159f6e539d292b01105b3a5ab3e7ac04782->enter($__internal_6d11b5e323a83f746cf46c968c40c159f6e539d292b01105b3a5ab3e7ac04782_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "        <h1>Here is all about ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["Product"] ?? $this->getContext($context, "Product")), "name", array()), "html", null, true);
        echo " </h1>
        <a href=\"";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_products_index");
        echo "\">All products</a>
        <h2>Details:</h2>
        <p><b>Name:</b> ";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["Product"] ?? $this->getContext($context, "Product")), "name", array()), "html", null, true);
        echo "</p>
        <p><b>Description:</b> ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["Product"] ?? $this->getContext($context, "Product")), "description", array()), "html", null, true);
        echo "</p>
        <p><b>Price:</b> ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["Product"] ?? $this->getContext($context, "Product")), "price", array()), "html", null, true);
        echo "</p>
    ";
        
        $__internal_6d11b5e323a83f746cf46c968c40c159f6e539d292b01105b3a5ab3e7ac04782->leave($__internal_6d11b5e323a83f746cf46c968c40c159f6e539d292b01105b3a5ab3e7ac04782_prof);

        
        $__internal_da54eefe4dbb70e560e1cf60cda249b2b46509a0e7fd5ef103f3a14070e0f1d3->leave($__internal_da54eefe4dbb70e560e1cf60cda249b2b46509a0e7fd5ef103f3a14070e0f1d3_prof);

    }

    public function getTemplateName()
    {
        return "details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 10,  63 => 9,  59 => 8,  54 => 6,  49 => 5,  40 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"AppBundle::main.html.twig\" %}


    {% block content %}
        <h1>Here is all about {{ Product.name }} </h1>
        <a href=\"{{ path('app_products_index') }}\">All products</a>
        <h2>Details:</h2>
        <p><b>Name:</b> {{ Product.name }}</p>
        <p><b>Description:</b> {{ Product.description }}</p>
        <p><b>Price:</b> {{ Product.price }}</p>
    {% endblock %}", "details.html.twig", "/var/www/html/ex63/hw63/app/Resources/views/details.html.twig");
    }
}
