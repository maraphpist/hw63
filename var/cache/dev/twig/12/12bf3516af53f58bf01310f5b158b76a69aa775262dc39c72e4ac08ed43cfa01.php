<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_266692687c2b6b63ca53b4f535837c51c77ace6673b2d225a8da804ba987c01f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ffabf787327b337ac9e6e840ec4f9e81259ecb4ff651a7be9cd38eb3e9d256a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ffabf787327b337ac9e6e840ec4f9e81259ecb4ff651a7be9cd38eb3e9d256a->enter($__internal_2ffabf787327b337ac9e6e840ec4f9e81259ecb4ff651a7be9cd38eb3e9d256a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $__internal_6da472562646090603ab6180ef6cfb86c6b453aeaef6d775ceb49e377acd4861 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6da472562646090603ab6180ef6cfb86c6b453aeaef6d775ceb49e377acd4861->enter($__internal_6da472562646090603ab6180ef6cfb86c6b453aeaef6d775ceb49e377acd4861_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo ($context["status_code"] ?? $this->getContext($context, "status_code"));
        echo " ";
        echo ($context["status_text"] ?? $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_2ffabf787327b337ac9e6e840ec4f9e81259ecb4ff651a7be9cd38eb3e9d256a->leave($__internal_2ffabf787327b337ac9e6e840ec4f9e81259ecb4ff651a7be9cd38eb3e9d256a_prof);

        
        $__internal_6da472562646090603ab6180ef6cfb86c6b453aeaef6d775ceb49e377acd4861->leave($__internal_6da472562646090603ab6180ef6cfb86c6b453aeaef6d775ceb49e377acd4861_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}
