<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_e86572db1a92ab303048537ee4700b8a0cd2dfa2eadb7dccc3536c68e5759eae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aead52795dc83828b11d72cc49ef02129c463f598502524c177d39c0849cfd96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aead52795dc83828b11d72cc49ef02129c463f598502524c177d39c0849cfd96->enter($__internal_aead52795dc83828b11d72cc49ef02129c463f598502524c177d39c0849cfd96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_02727b8193ea772e2a381fb2ba8cf317c426b09b964c0b8d3607999742f36901 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02727b8193ea772e2a381fb2ba8cf317c426b09b964c0b8d3607999742f36901->enter($__internal_02727b8193ea772e2a381fb2ba8cf317c426b09b964c0b8d3607999742f36901_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_aead52795dc83828b11d72cc49ef02129c463f598502524c177d39c0849cfd96->leave($__internal_aead52795dc83828b11d72cc49ef02129c463f598502524c177d39c0849cfd96_prof);

        
        $__internal_02727b8193ea772e2a381fb2ba8cf317c426b09b964c0b8d3607999742f36901->leave($__internal_02727b8193ea772e2a381fb2ba8cf317c426b09b964c0b8d3607999742f36901_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}
