<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_d55ec6d5b6658f9ec0f205de403505345fe05864bfb061d69827ba06242d13bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e15b432b07453fdb21cc02d88c8d016657b9908aab8d380eff290df87d1dc078 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e15b432b07453fdb21cc02d88c8d016657b9908aab8d380eff290df87d1dc078->enter($__internal_e15b432b07453fdb21cc02d88c8d016657b9908aab8d380eff290df87d1dc078_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_4aa03bb1d77bf1797c76dc682cb020b8314ddc0c8b91c5c4a8c501b305caac1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4aa03bb1d77bf1797c76dc682cb020b8314ddc0c8b91c5c4a8c501b305caac1a->enter($__internal_4aa03bb1d77bf1797c76dc682cb020b8314ddc0c8b91c5c4a8c501b305caac1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e15b432b07453fdb21cc02d88c8d016657b9908aab8d380eff290df87d1dc078->leave($__internal_e15b432b07453fdb21cc02d88c8d016657b9908aab8d380eff290df87d1dc078_prof);

        
        $__internal_4aa03bb1d77bf1797c76dc682cb020b8314ddc0c8b91c5c4a8c501b305caac1a->leave($__internal_4aa03bb1d77bf1797c76dc682cb020b8314ddc0c8b91c5c4a8c501b305caac1a_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_23b6e1a0a7d46261e5bc277b701cebd75b68baa190bc080d75be9a775a6c6a9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23b6e1a0a7d46261e5bc277b701cebd75b68baa190bc080d75be9a775a6c6a9e->enter($__internal_23b6e1a0a7d46261e5bc277b701cebd75b68baa190bc080d75be9a775a6c6a9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_9533cbdba07803e5e352523b63a7fb159711aa2cac60157efcf6bc7012811db9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9533cbdba07803e5e352523b63a7fb159711aa2cac60157efcf6bc7012811db9->enter($__internal_9533cbdba07803e5e352523b63a7fb159711aa2cac60157efcf6bc7012811db9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_9533cbdba07803e5e352523b63a7fb159711aa2cac60157efcf6bc7012811db9->leave($__internal_9533cbdba07803e5e352523b63a7fb159711aa2cac60157efcf6bc7012811db9_prof);

        
        $__internal_23b6e1a0a7d46261e5bc277b701cebd75b68baa190bc080d75be9a775a6c6a9e->leave($__internal_23b6e1a0a7d46261e5bc277b701cebd75b68baa190bc080d75be9a775a6c6a9e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
