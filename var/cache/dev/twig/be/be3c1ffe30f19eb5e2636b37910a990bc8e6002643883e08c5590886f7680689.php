<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_268d95beb4061e727eea787c5670fa28b7c2b616fc053ccd560ff1dd528c3083 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99b075de7cd59a296add34b619f993401ddb8fe5ff9ad634d7c0e7c81e9cb8e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99b075de7cd59a296add34b619f993401ddb8fe5ff9ad634d7c0e7c81e9cb8e7->enter($__internal_99b075de7cd59a296add34b619f993401ddb8fe5ff9ad634d7c0e7c81e9cb8e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_2c05d0f7eadb1ba2deeca5f4717d6dd2ed3099fad9701443f5d0d121da7a647c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c05d0f7eadb1ba2deeca5f4717d6dd2ed3099fad9701443f5d0d121da7a647c->enter($__internal_2c05d0f7eadb1ba2deeca5f4717d6dd2ed3099fad9701443f5d0d121da7a647c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_99b075de7cd59a296add34b619f993401ddb8fe5ff9ad634d7c0e7c81e9cb8e7->leave($__internal_99b075de7cd59a296add34b619f993401ddb8fe5ff9ad634d7c0e7c81e9cb8e7_prof);

        
        $__internal_2c05d0f7eadb1ba2deeca5f4717d6dd2ed3099fad9701443f5d0d121da7a647c->leave($__internal_2c05d0f7eadb1ba2deeca5f4717d6dd2ed3099fad9701443f5d0d121da7a647c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
