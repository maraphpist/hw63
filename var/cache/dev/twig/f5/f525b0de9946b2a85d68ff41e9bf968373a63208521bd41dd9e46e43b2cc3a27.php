<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_3940b12f38f4f22cd7582c0ade22f69829cb0a3d02a402a00fb82a273828a6d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a10e71f63a6fb1ad8485824807d9f89202fc725b959b6be44955396f641c1e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a10e71f63a6fb1ad8485824807d9f89202fc725b959b6be44955396f641c1e0->enter($__internal_8a10e71f63a6fb1ad8485824807d9f89202fc725b959b6be44955396f641c1e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_52e9900b21f3a03ac88b15936b46bfcf6c3b1ae719bf726f8f71294b699aeb38 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52e9900b21f3a03ac88b15936b46bfcf6c3b1ae719bf726f8f71294b699aeb38->enter($__internal_52e9900b21f3a03ac88b15936b46bfcf6c3b1ae719bf726f8f71294b699aeb38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8a10e71f63a6fb1ad8485824807d9f89202fc725b959b6be44955396f641c1e0->leave($__internal_8a10e71f63a6fb1ad8485824807d9f89202fc725b959b6be44955396f641c1e0_prof);

        
        $__internal_52e9900b21f3a03ac88b15936b46bfcf6c3b1ae719bf726f8f71294b699aeb38->leave($__internal_52e9900b21f3a03ac88b15936b46bfcf6c3b1ae719bf726f8f71294b699aeb38_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_efc63b15bec415ddfd6bb7dd5ba7c93db5502b9904f9a74ae7560f37b24eedc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_efc63b15bec415ddfd6bb7dd5ba7c93db5502b9904f9a74ae7560f37b24eedc5->enter($__internal_efc63b15bec415ddfd6bb7dd5ba7c93db5502b9904f9a74ae7560f37b24eedc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ae3b86f253203d70d4842a231456a7b1a6ac8b58f018783472686a8c3452aa3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae3b86f253203d70d4842a231456a7b1a6ac8b58f018783472686a8c3452aa3e->enter($__internal_ae3b86f253203d70d4842a231456a7b1a6ac8b58f018783472686a8c3452aa3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_ae3b86f253203d70d4842a231456a7b1a6ac8b58f018783472686a8c3452aa3e->leave($__internal_ae3b86f253203d70d4842a231456a7b1a6ac8b58f018783472686a8c3452aa3e_prof);

        
        $__internal_efc63b15bec415ddfd6bb7dd5ba7c93db5502b9904f9a74ae7560f37b24eedc5->leave($__internal_efc63b15bec415ddfd6bb7dd5ba7c93db5502b9904f9a74ae7560f37b24eedc5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
