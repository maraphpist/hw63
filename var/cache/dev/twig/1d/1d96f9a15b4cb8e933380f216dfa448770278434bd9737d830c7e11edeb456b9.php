<?php

/* default/details.html.twig */
class __TwigTemplate_7d70be2141886995123badee1532c74eb4a9d44aa14b805a5a6baedf8516c239 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("main.html.twig", "default/details.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "main.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b785e981319574e76f59a9ad85703a41081df889bcb43c8ab221c19a04ddd21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b785e981319574e76f59a9ad85703a41081df889bcb43c8ab221c19a04ddd21->enter($__internal_5b785e981319574e76f59a9ad85703a41081df889bcb43c8ab221c19a04ddd21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/details.html.twig"));

        $__internal_0db1b4923257539b3890273e05a312041469eaf08e08482e0c53a2b38314bf46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0db1b4923257539b3890273e05a312041469eaf08e08482e0c53a2b38314bf46->enter($__internal_0db1b4923257539b3890273e05a312041469eaf08e08482e0c53a2b38314bf46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/details.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5b785e981319574e76f59a9ad85703a41081df889bcb43c8ab221c19a04ddd21->leave($__internal_5b785e981319574e76f59a9ad85703a41081df889bcb43c8ab221c19a04ddd21_prof);

        
        $__internal_0db1b4923257539b3890273e05a312041469eaf08e08482e0c53a2b38314bf46->leave($__internal_0db1b4923257539b3890273e05a312041469eaf08e08482e0c53a2b38314bf46_prof);

    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        $__internal_69df341044f77335a8f41b5881e4ae32f216616a18f981634dc59432847db6b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69df341044f77335a8f41b5881e4ae32f216616a18f981634dc59432847db6b7->enter($__internal_69df341044f77335a8f41b5881e4ae32f216616a18f981634dc59432847db6b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_8a67a054dba4cf6a7bba2fde365aa1fe9e80585a93a5612321449e6d410a2209 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a67a054dba4cf6a7bba2fde365aa1fe9e80585a93a5612321449e6d410a2209->enter($__internal_8a67a054dba4cf6a7bba2fde365aa1fe9e80585a93a5612321449e6d410a2209_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "        <h1>Here is all about ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["Category"] ?? $this->getContext($context, "Category")), "id", array()), "html", null, true);
        echo " </h1>
        <a href=\"\">All products</a>
        <h2>Details:</h2>
        <p><b>Name:</b></p>
        <p><b>Description:</b></p>
    ";
        
        $__internal_8a67a054dba4cf6a7bba2fde365aa1fe9e80585a93a5612321449e6d410a2209->leave($__internal_8a67a054dba4cf6a7bba2fde365aa1fe9e80585a93a5612321449e6d410a2209_prof);

        
        $__internal_69df341044f77335a8f41b5881e4ae32f216616a18f981634dc59432847db6b7->leave($__internal_69df341044f77335a8f41b5881e4ae32f216616a18f981634dc59432847db6b7_prof);

    }

    public function getTemplateName()
    {
        return "default/details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 5,  40 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"main.html.twig\" %}


    {% block content %}
        <h1>Here is all about {{ Category.id }} </h1>
        <a href=\"\">All products</a>
        <h2>Details:</h2>
        <p><b>Name:</b></p>
        <p><b>Description:</b></p>
    {% endblock %}", "default/details.html.twig", "/var/www/html/ex63/hw63/app/Resources/views/default/details.html.twig");
    }
}
