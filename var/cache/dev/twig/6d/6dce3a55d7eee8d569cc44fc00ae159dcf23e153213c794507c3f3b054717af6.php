<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_1d248dcab04f8cbde7f9f83d30aa6fe4d619c84bcc9614f7da894e29a15f9d8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d3821b50375dc74744d73a068a30f611c12426fd064969c62d5bb3a32ee94d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d3821b50375dc74744d73a068a30f611c12426fd064969c62d5bb3a32ee94d3->enter($__internal_5d3821b50375dc74744d73a068a30f611c12426fd064969c62d5bb3a32ee94d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_92dba921e15aeece1ab6a49b8750c23852519f33af1ae7eebf67f2459072ad65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92dba921e15aeece1ab6a49b8750c23852519f33af1ae7eebf67f2459072ad65->enter($__internal_92dba921e15aeece1ab6a49b8750c23852519f33af1ae7eebf67f2459072ad65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_5d3821b50375dc74744d73a068a30f611c12426fd064969c62d5bb3a32ee94d3->leave($__internal_5d3821b50375dc74744d73a068a30f611c12426fd064969c62d5bb3a32ee94d3_prof);

        
        $__internal_92dba921e15aeece1ab6a49b8750c23852519f33af1ae7eebf67f2459072ad65->leave($__internal_92dba921e15aeece1ab6a49b8750c23852519f33af1ae7eebf67f2459072ad65_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
