<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_0a150352ead77b8f4825c5cb64eb12a87033c68131590abccc15d5143742aa77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67342b363d24432d7d2007140f1b003dd7e394ffe529a925a6fa758e16b9b135 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67342b363d24432d7d2007140f1b003dd7e394ffe529a925a6fa758e16b9b135->enter($__internal_67342b363d24432d7d2007140f1b003dd7e394ffe529a925a6fa758e16b9b135_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_7e49a3c19a3a78533d0f894ecc831b042e0a0e47273e52ba3265b8f7ca512f4f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e49a3c19a3a78533d0f894ecc831b042e0a0e47273e52ba3265b8f7ca512f4f->enter($__internal_7e49a3c19a3a78533d0f894ecc831b042e0a0e47273e52ba3265b8f7ca512f4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_67342b363d24432d7d2007140f1b003dd7e394ffe529a925a6fa758e16b9b135->leave($__internal_67342b363d24432d7d2007140f1b003dd7e394ffe529a925a6fa758e16b9b135_prof);

        
        $__internal_7e49a3c19a3a78533d0f894ecc831b042e0a0e47273e52ba3265b8f7ca512f4f->leave($__internal_7e49a3c19a3a78533d0f894ecc831b042e0a0e47273e52ba3265b8f7ca512f4f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
