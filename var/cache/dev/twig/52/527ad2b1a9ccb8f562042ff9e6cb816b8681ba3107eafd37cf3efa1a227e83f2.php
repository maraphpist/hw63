<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_624075785c095260ceab89457b4dac6a48a241898921aeb6cd8257a204ee9b36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_253cc8bf7abf5a577f27268bfe70ce8c2202af8b33a32f78f0d09f5943857a1c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_253cc8bf7abf5a577f27268bfe70ce8c2202af8b33a32f78f0d09f5943857a1c->enter($__internal_253cc8bf7abf5a577f27268bfe70ce8c2202af8b33a32f78f0d09f5943857a1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_0f241146bb2dc47a9f8c48026e917964a06e2641e6f5f3fd598caf56bb0f56fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f241146bb2dc47a9f8c48026e917964a06e2641e6f5f3fd598caf56bb0f56fe->enter($__internal_0f241146bb2dc47a9f8c48026e917964a06e2641e6f5f3fd598caf56bb0f56fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_253cc8bf7abf5a577f27268bfe70ce8c2202af8b33a32f78f0d09f5943857a1c->leave($__internal_253cc8bf7abf5a577f27268bfe70ce8c2202af8b33a32f78f0d09f5943857a1c_prof);

        
        $__internal_0f241146bb2dc47a9f8c48026e917964a06e2641e6f5f3fd598caf56bb0f56fe->leave($__internal_0f241146bb2dc47a9f8c48026e917964a06e2641e6f5f3fd598caf56bb0f56fe_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
