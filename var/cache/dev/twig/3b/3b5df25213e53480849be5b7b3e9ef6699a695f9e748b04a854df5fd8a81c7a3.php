<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_67ca4283ac4c513189ba611cf0e68525ccf5da3ed53f5d19ce9451d14d61a2bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_931fb6cb1a59000dfa0825ec24ed5f7469c437f1e03e8d3d78d7d8ae1c661199 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_931fb6cb1a59000dfa0825ec24ed5f7469c437f1e03e8d3d78d7d8ae1c661199->enter($__internal_931fb6cb1a59000dfa0825ec24ed5f7469c437f1e03e8d3d78d7d8ae1c661199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_e58c9dcf3cdd624a34ec027f5b17f1016a950668361e32932551f833e1e3da21 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e58c9dcf3cdd624a34ec027f5b17f1016a950668361e32932551f833e1e3da21->enter($__internal_e58c9dcf3cdd624a34ec027f5b17f1016a950668361e32932551f833e1e3da21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_931fb6cb1a59000dfa0825ec24ed5f7469c437f1e03e8d3d78d7d8ae1c661199->leave($__internal_931fb6cb1a59000dfa0825ec24ed5f7469c437f1e03e8d3d78d7d8ae1c661199_prof);

        
        $__internal_e58c9dcf3cdd624a34ec027f5b17f1016a950668361e32932551f833e1e3da21->leave($__internal_e58c9dcf3cdd624a34ec027f5b17f1016a950668361e32932551f833e1e3da21_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
