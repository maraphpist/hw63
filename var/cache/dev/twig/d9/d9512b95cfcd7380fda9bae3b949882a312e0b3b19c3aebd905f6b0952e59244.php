<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_83308ca1b8ae09e2e8878d149b1eb814f2c3872d06b0ece4682fea359edcc633 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4246f415d90dc867a343100b613149f1eadfd1df169c3472b96502373a6ede61 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4246f415d90dc867a343100b613149f1eadfd1df169c3472b96502373a6ede61->enter($__internal_4246f415d90dc867a343100b613149f1eadfd1df169c3472b96502373a6ede61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_a4352c24f4558d6fd9f3e51e6e21c750c318e9c071e15717f9fa65750a5ef45e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4352c24f4558d6fd9f3e51e6e21c750c318e9c071e15717f9fa65750a5ef45e->enter($__internal_a4352c24f4558d6fd9f3e51e6e21c750c318e9c071e15717f9fa65750a5ef45e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4246f415d90dc867a343100b613149f1eadfd1df169c3472b96502373a6ede61->leave($__internal_4246f415d90dc867a343100b613149f1eadfd1df169c3472b96502373a6ede61_prof);

        
        $__internal_a4352c24f4558d6fd9f3e51e6e21c750c318e9c071e15717f9fa65750a5ef45e->leave($__internal_a4352c24f4558d6fd9f3e51e6e21c750c318e9c071e15717f9fa65750a5ef45e_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ab719a2057a1e94e90b066d7f1d8b204f25baa640edea1fc83d9ba81e5257d3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab719a2057a1e94e90b066d7f1d8b204f25baa640edea1fc83d9ba81e5257d3d->enter($__internal_ab719a2057a1e94e90b066d7f1d8b204f25baa640edea1fc83d9ba81e5257d3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_4411e589ba3e75627bd56b2849085e1b3e7b965fac92e2d50e5d4c4022fc029a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4411e589ba3e75627bd56b2849085e1b3e7b965fac92e2d50e5d4c4022fc029a->enter($__internal_4411e589ba3e75627bd56b2849085e1b3e7b965fac92e2d50e5d4c4022fc029a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_4411e589ba3e75627bd56b2849085e1b3e7b965fac92e2d50e5d4c4022fc029a->leave($__internal_4411e589ba3e75627bd56b2849085e1b3e7b965fac92e2d50e5d4c4022fc029a_prof);

        
        $__internal_ab719a2057a1e94e90b066d7f1d8b204f25baa640edea1fc83d9ba81e5257d3d->leave($__internal_ab719a2057a1e94e90b066d7f1d8b204f25baa640edea1fc83d9ba81e5257d3d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_1489de1476f1c952e777c56ee3856b4afc7ba57b2a494ba548d784a6caf305b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1489de1476f1c952e777c56ee3856b4afc7ba57b2a494ba548d784a6caf305b8->enter($__internal_1489de1476f1c952e777c56ee3856b4afc7ba57b2a494ba548d784a6caf305b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d98b06677f9a3e61d63c20c4d0ef0060f2f376996e799061f1db8cea1c9773c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d98b06677f9a3e61d63c20c4d0ef0060f2f376996e799061f1db8cea1c9773c0->enter($__internal_d98b06677f9a3e61d63c20c4d0ef0060f2f376996e799061f1db8cea1c9773c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_d98b06677f9a3e61d63c20c4d0ef0060f2f376996e799061f1db8cea1c9773c0->leave($__internal_d98b06677f9a3e61d63c20c4d0ef0060f2f376996e799061f1db8cea1c9773c0_prof);

        
        $__internal_1489de1476f1c952e777c56ee3856b4afc7ba57b2a494ba548d784a6caf305b8->leave($__internal_1489de1476f1c952e777c56ee3856b4afc7ba57b2a494ba548d784a6caf305b8_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
