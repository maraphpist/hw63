<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_11e5710c6af86f6f6ce133a04312d2493e47fd7fe737533a5296fb25c92db98b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fc372e39316cdde63bb4956fa5fb68da9c924853e295f8ff90ac483d2696177b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc372e39316cdde63bb4956fa5fb68da9c924853e295f8ff90ac483d2696177b->enter($__internal_fc372e39316cdde63bb4956fa5fb68da9c924853e295f8ff90ac483d2696177b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_e6af2ba60650137fe489e9508d3dabf5e1dc9edd28b8ffaab8a3d6fed8681995 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e6af2ba60650137fe489e9508d3dabf5e1dc9edd28b8ffaab8a3d6fed8681995->enter($__internal_e6af2ba60650137fe489e9508d3dabf5e1dc9edd28b8ffaab8a3d6fed8681995_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_fc372e39316cdde63bb4956fa5fb68da9c924853e295f8ff90ac483d2696177b->leave($__internal_fc372e39316cdde63bb4956fa5fb68da9c924853e295f8ff90ac483d2696177b_prof);

        
        $__internal_e6af2ba60650137fe489e9508d3dabf5e1dc9edd28b8ffaab8a3d6fed8681995->leave($__internal_e6af2ba60650137fe489e9508d3dabf5e1dc9edd28b8ffaab8a3d6fed8681995_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
