<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_8f797635c6560fcf0931d89d6d05cc91c8d4cbbe7d51be4498a2d1830255611d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35ae8d7311ee040139ba956d79e9edbf9dc07fc7fc1e14e755218ecca2bb6a16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35ae8d7311ee040139ba956d79e9edbf9dc07fc7fc1e14e755218ecca2bb6a16->enter($__internal_35ae8d7311ee040139ba956d79e9edbf9dc07fc7fc1e14e755218ecca2bb6a16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_9a23780beda25ec40dfa2018841362e8f7cb3fc25357e6d02e172be8808127b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a23780beda25ec40dfa2018841362e8f7cb3fc25357e6d02e172be8808127b9->enter($__internal_9a23780beda25ec40dfa2018841362e8f7cb3fc25357e6d02e172be8808127b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35ae8d7311ee040139ba956d79e9edbf9dc07fc7fc1e14e755218ecca2bb6a16->leave($__internal_35ae8d7311ee040139ba956d79e9edbf9dc07fc7fc1e14e755218ecca2bb6a16_prof);

        
        $__internal_9a23780beda25ec40dfa2018841362e8f7cb3fc25357e6d02e172be8808127b9->leave($__internal_9a23780beda25ec40dfa2018841362e8f7cb3fc25357e6d02e172be8808127b9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3f3d78729e5b71ff7ea14023a78dda139674dd90c8ded9e1a1698aeee7b16b88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f3d78729e5b71ff7ea14023a78dda139674dd90c8ded9e1a1698aeee7b16b88->enter($__internal_3f3d78729e5b71ff7ea14023a78dda139674dd90c8ded9e1a1698aeee7b16b88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_90b9c45f93734fc6bb356edd1884bb08a535e044f32697abd33101cb487b0d84 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90b9c45f93734fc6bb356edd1884bb08a535e044f32697abd33101cb487b0d84->enter($__internal_90b9c45f93734fc6bb356edd1884bb08a535e044f32697abd33101cb487b0d84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_90b9c45f93734fc6bb356edd1884bb08a535e044f32697abd33101cb487b0d84->leave($__internal_90b9c45f93734fc6bb356edd1884bb08a535e044f32697abd33101cb487b0d84_prof);

        
        $__internal_3f3d78729e5b71ff7ea14023a78dda139674dd90c8ded9e1a1698aeee7b16b88->leave($__internal_3f3d78729e5b71ff7ea14023a78dda139674dd90c8ded9e1a1698aeee7b16b88_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
