<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_fb7333f16a6803cc6226c73ee2f5f0e9486aaa095366b8f853b49a2b908671e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3a7d09323b65dff41bef5bb1793cf0d35f8f7f32d849c110abaacce4b3c41694 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a7d09323b65dff41bef5bb1793cf0d35f8f7f32d849c110abaacce4b3c41694->enter($__internal_3a7d09323b65dff41bef5bb1793cf0d35f8f7f32d849c110abaacce4b3c41694_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_e55358d77acd8497085819c8d5c08594eef1a2df90d5baeb61b1475375c71d63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e55358d77acd8497085819c8d5c08594eef1a2df90d5baeb61b1475375c71d63->enter($__internal_e55358d77acd8497085819c8d5c08594eef1a2df90d5baeb61b1475375c71d63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3a7d09323b65dff41bef5bb1793cf0d35f8f7f32d849c110abaacce4b3c41694->leave($__internal_3a7d09323b65dff41bef5bb1793cf0d35f8f7f32d849c110abaacce4b3c41694_prof);

        
        $__internal_e55358d77acd8497085819c8d5c08594eef1a2df90d5baeb61b1475375c71d63->leave($__internal_e55358d77acd8497085819c8d5c08594eef1a2df90d5baeb61b1475375c71d63_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fc67e3cda3f6b0ac51d06f90975dfd11cdbca3dabbcfeed484a359451362f6ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc67e3cda3f6b0ac51d06f90975dfd11cdbca3dabbcfeed484a359451362f6ef->enter($__internal_fc67e3cda3f6b0ac51d06f90975dfd11cdbca3dabbcfeed484a359451362f6ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_37341653a0be8b111c2a312934d96371107f389e8a84ea836518214d49ec8a60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37341653a0be8b111c2a312934d96371107f389e8a84ea836518214d49ec8a60->enter($__internal_37341653a0be8b111c2a312934d96371107f389e8a84ea836518214d49ec8a60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_37341653a0be8b111c2a312934d96371107f389e8a84ea836518214d49ec8a60->leave($__internal_37341653a0be8b111c2a312934d96371107f389e8a84ea836518214d49ec8a60_prof);

        
        $__internal_fc67e3cda3f6b0ac51d06f90975dfd11cdbca3dabbcfeed484a359451362f6ef->leave($__internal_fc67e3cda3f6b0ac51d06f90975dfd11cdbca3dabbcfeed484a359451362f6ef_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
