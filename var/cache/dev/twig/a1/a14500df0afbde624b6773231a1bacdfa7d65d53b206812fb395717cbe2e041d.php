<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_bc4ea1beb28828ce335227ee50b2e3d5971fbb5a697333240a240d7c558d8474 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7721b157014ead6739b24f5d3bacd94fa44c23324b4bfc557bcf6737676c50c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7721b157014ead6739b24f5d3bacd94fa44c23324b4bfc557bcf6737676c50c7->enter($__internal_7721b157014ead6739b24f5d3bacd94fa44c23324b4bfc557bcf6737676c50c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_1a45d76faad7c83f2b6bf6416e091a350ce39ed841c7b3bde93ad4e1ec784c18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a45d76faad7c83f2b6bf6416e091a350ce39ed841c7b3bde93ad4e1ec784c18->enter($__internal_1a45d76faad7c83f2b6bf6416e091a350ce39ed841c7b3bde93ad4e1ec784c18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7721b157014ead6739b24f5d3bacd94fa44c23324b4bfc557bcf6737676c50c7->leave($__internal_7721b157014ead6739b24f5d3bacd94fa44c23324b4bfc557bcf6737676c50c7_prof);

        
        $__internal_1a45d76faad7c83f2b6bf6416e091a350ce39ed841c7b3bde93ad4e1ec784c18->leave($__internal_1a45d76faad7c83f2b6bf6416e091a350ce39ed841c7b3bde93ad4e1ec784c18_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b941239e8819dcdf9835a68971871674f0799bbeb97508d91103e7a878b15573 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b941239e8819dcdf9835a68971871674f0799bbeb97508d91103e7a878b15573->enter($__internal_b941239e8819dcdf9835a68971871674f0799bbeb97508d91103e7a878b15573_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6c7079e506dba72f1bb265a3813f8baad870cf5cc6542db0d57f102cea30a31c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c7079e506dba72f1bb265a3813f8baad870cf5cc6542db0d57f102cea30a31c->enter($__internal_6c7079e506dba72f1bb265a3813f8baad870cf5cc6542db0d57f102cea30a31c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_6c7079e506dba72f1bb265a3813f8baad870cf5cc6542db0d57f102cea30a31c->leave($__internal_6c7079e506dba72f1bb265a3813f8baad870cf5cc6542db0d57f102cea30a31c_prof);

        
        $__internal_b941239e8819dcdf9835a68971871674f0799bbeb97508d91103e7a878b15573->leave($__internal_b941239e8819dcdf9835a68971871674f0799bbeb97508d91103e7a878b15573_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
