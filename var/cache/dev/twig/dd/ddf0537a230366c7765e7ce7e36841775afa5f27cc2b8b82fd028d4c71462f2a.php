<?php

/* main.html.twig */
class __TwigTemplate_8e766d4c3187934fec7632301b650ca2b9dacf1feddfb49e5043d0b021b15b1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "main.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_989880c87e8fb3a000e03962502f64f3808c50cea99415a0ce420d03f1f49b58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_989880c87e8fb3a000e03962502f64f3808c50cea99415a0ce420d03f1f49b58->enter($__internal_989880c87e8fb3a000e03962502f64f3808c50cea99415a0ce420d03f1f49b58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "main.html.twig"));

        $__internal_5f5b3eda52854eea7fd63de2af75aa23f30b70ddc39a014a8db02184b94cd630 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f5b3eda52854eea7fd63de2af75aa23f30b70ddc39a014a8db02184b94cd630->enter($__internal_5f5b3eda52854eea7fd63de2af75aa23f30b70ddc39a014a8db02184b94cd630_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "main.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_989880c87e8fb3a000e03962502f64f3808c50cea99415a0ce420d03f1f49b58->leave($__internal_989880c87e8fb3a000e03962502f64f3808c50cea99415a0ce420d03f1f49b58_prof);

        
        $__internal_5f5b3eda52854eea7fd63de2af75aa23f30b70ddc39a014a8db02184b94cd630->leave($__internal_5f5b3eda52854eea7fd63de2af75aa23f30b70ddc39a014a8db02184b94cd630_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_7dfccacf34e367fb56b111cfde53e371ec18a7f32698f8b3c235dc7e9eb0368d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7dfccacf34e367fb56b111cfde53e371ec18a7f32698f8b3c235dc7e9eb0368d->enter($__internal_7dfccacf34e367fb56b111cfde53e371ec18a7f32698f8b3c235dc7e9eb0368d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ea9a0477cec47363baa9d89ee25af9cc9b92cafadcdd223de521c3f30d4addde = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea9a0477cec47363baa9d89ee25af9cc9b92cafadcdd223de521c3f30d4addde->enter($__internal_ea9a0477cec47363baa9d89ee25af9cc9b92cafadcdd223de521c3f30d4addde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"navbar navbar-default\">
        <ul class=\"nav navbar-nav\">
            <li>
                <a href=\"";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Home</a>
            </li>
            <li>
                <a href=\"#\">About</a>
            </li>
            <li>
                <a href=\"#\">Contacts</a>
            </li>
        </ul>
    </div>

";
        
        $__internal_ea9a0477cec47363baa9d89ee25af9cc9b92cafadcdd223de521c3f30d4addde->leave($__internal_ea9a0477cec47363baa9d89ee25af9cc9b92cafadcdd223de521c3f30d4addde_prof);

        
        $__internal_7dfccacf34e367fb56b111cfde53e371ec18a7f32698f8b3c235dc7e9eb0368d->leave($__internal_7dfccacf34e367fb56b111cfde53e371ec18a7f32698f8b3c235dc7e9eb0368d_prof);

    }

    // line 18
    public function block_content($context, array $blocks = array())
    {
        $__internal_4b77e7a33b9368b9b9ac3a58d22a852cdb4dddca5cfd86760583a8e51d938002 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b77e7a33b9368b9b9ac3a58d22a852cdb4dddca5cfd86760583a8e51d938002->enter($__internal_4b77e7a33b9368b9b9ac3a58d22a852cdb4dddca5cfd86760583a8e51d938002_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_f3712c1d7cf70a6ec75e3baf125d730ad1fcf4ac032bd72b4c51671413362d92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3712c1d7cf70a6ec75e3baf125d730ad1fcf4ac032bd72b4c51671413362d92->enter($__internal_f3712c1d7cf70a6ec75e3baf125d730ad1fcf4ac032bd72b4c51671413362d92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_f3712c1d7cf70a6ec75e3baf125d730ad1fcf4ac032bd72b4c51671413362d92->leave($__internal_f3712c1d7cf70a6ec75e3baf125d730ad1fcf4ac032bd72b4c51671413362d92_prof);

        
        $__internal_4b77e7a33b9368b9b9ac3a58d22a852cdb4dddca5cfd86760583a8e51d938002->leave($__internal_4b77e7a33b9368b9b9ac3a58d22a852cdb4dddca5cfd86760583a8e51d938002_prof);

    }

    public function getTemplateName()
    {
        return "main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 18,  55 => 6,  50 => 3,  41 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}
{% block body %}
    <div class=\"navbar navbar-default\">
        <ul class=\"nav navbar-nav\">
            <li>
                <a href=\"{{ path('homepage') }}\">Home</a>
            </li>
            <li>
                <a href=\"#\">About</a>
            </li>
            <li>
                <a href=\"#\">Contacts</a>
            </li>
        </ul>
    </div>

{% endblock %}
 {% block content %}{% endblock %}", "main.html.twig", "/var/www/html/ex63/hw63/app/Resources/views/main.html.twig");
    }
}
