<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_e12ba50100984e4be16524ff0b359558caf5114763600052ebc819e4740ad228 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f3400c46fae1e7d6b1aaf750f2b51ac6f9ee8a92f4a7431c20a69661df861dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f3400c46fae1e7d6b1aaf750f2b51ac6f9ee8a92f4a7431c20a69661df861dd->enter($__internal_6f3400c46fae1e7d6b1aaf750f2b51ac6f9ee8a92f4a7431c20a69661df861dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_792a1faf82147a2b4c2ea7aacfa63b402f11ad7d0702e5f2cc413ef3e71e1b41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_792a1faf82147a2b4c2ea7aacfa63b402f11ad7d0702e5f2cc413ef3e71e1b41->enter($__internal_792a1faf82147a2b4c2ea7aacfa63b402f11ad7d0702e5f2cc413ef3e71e1b41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_6f3400c46fae1e7d6b1aaf750f2b51ac6f9ee8a92f4a7431c20a69661df861dd->leave($__internal_6f3400c46fae1e7d6b1aaf750f2b51ac6f9ee8a92f4a7431c20a69661df861dd_prof);

        
        $__internal_792a1faf82147a2b4c2ea7aacfa63b402f11ad7d0702e5f2cc413ef3e71e1b41->leave($__internal_792a1faf82147a2b4c2ea7aacfa63b402f11ad7d0702e5f2cc413ef3e71e1b41_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
