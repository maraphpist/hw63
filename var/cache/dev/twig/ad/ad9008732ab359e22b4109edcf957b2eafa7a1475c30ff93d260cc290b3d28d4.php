<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_d9e1981aa8e284b05e9caf5b759e27b33c515389824ce30e73ce4ddfcc60ec9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbcd1651958a7033c64f837a4855fba88e8f0a7c183c0936d30ce1c7c9170d08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbcd1651958a7033c64f837a4855fba88e8f0a7c183c0936d30ce1c7c9170d08->enter($__internal_bbcd1651958a7033c64f837a4855fba88e8f0a7c183c0936d30ce1c7c9170d08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_7c81ceafc85368531e38ddfeeaeb114d6439c5597f9d7fc463584208b8a90550 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c81ceafc85368531e38ddfeeaeb114d6439c5597f9d7fc463584208b8a90550->enter($__internal_7c81ceafc85368531e38ddfeeaeb114d6439c5597f9d7fc463584208b8a90550_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bbcd1651958a7033c64f837a4855fba88e8f0a7c183c0936d30ce1c7c9170d08->leave($__internal_bbcd1651958a7033c64f837a4855fba88e8f0a7c183c0936d30ce1c7c9170d08_prof);

        
        $__internal_7c81ceafc85368531e38ddfeeaeb114d6439c5597f9d7fc463584208b8a90550->leave($__internal_7c81ceafc85368531e38ddfeeaeb114d6439c5597f9d7fc463584208b8a90550_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6856c688a89ad4a537ffc74cea5d7ae751496729838d09f7b83abef779fa26a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6856c688a89ad4a537ffc74cea5d7ae751496729838d09f7b83abef779fa26a2->enter($__internal_6856c688a89ad4a537ffc74cea5d7ae751496729838d09f7b83abef779fa26a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_dbcb5d323560c138eede374c39c54141bfdc17baad85c90fdaeb5455ac7828cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dbcb5d323560c138eede374c39c54141bfdc17baad85c90fdaeb5455ac7828cf->enter($__internal_dbcb5d323560c138eede374c39c54141bfdc17baad85c90fdaeb5455ac7828cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_dbcb5d323560c138eede374c39c54141bfdc17baad85c90fdaeb5455ac7828cf->leave($__internal_dbcb5d323560c138eede374c39c54141bfdc17baad85c90fdaeb5455ac7828cf_prof);

        
        $__internal_6856c688a89ad4a537ffc74cea5d7ae751496729838d09f7b83abef779fa26a2->leave($__internal_6856c688a89ad4a537ffc74cea5d7ae751496729838d09f7b83abef779fa26a2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
