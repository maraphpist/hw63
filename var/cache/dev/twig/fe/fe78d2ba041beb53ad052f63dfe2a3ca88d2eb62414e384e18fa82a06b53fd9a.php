<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_27fcc32cbc727b456cb6bdc4703e93e2968b841d299cdcc212c3aff97df9e93e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed96bd16eaa4b621706db24058f8e316988cb952f12ca4eac0a5085a91884230 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed96bd16eaa4b621706db24058f8e316988cb952f12ca4eac0a5085a91884230->enter($__internal_ed96bd16eaa4b621706db24058f8e316988cb952f12ca4eac0a5085a91884230_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_b6454e8fab13e06dfedd8f1af7ae7fb0c7245d401df1ff46d2ad67fe4539d4ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6454e8fab13e06dfedd8f1af7ae7fb0c7245d401df1ff46d2ad67fe4539d4ef->enter($__internal_b6454e8fab13e06dfedd8f1af7ae7fb0c7245d401df1ff46d2ad67fe4539d4ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_ed96bd16eaa4b621706db24058f8e316988cb952f12ca4eac0a5085a91884230->leave($__internal_ed96bd16eaa4b621706db24058f8e316988cb952f12ca4eac0a5085a91884230_prof);

        
        $__internal_b6454e8fab13e06dfedd8f1af7ae7fb0c7245d401df1ff46d2ad67fe4539d4ef->leave($__internal_b6454e8fab13e06dfedd8f1af7ae7fb0c7245d401df1ff46d2ad67fe4539d4ef_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_77a879c730c26ac9c89f5afbf4ef8a58f016682527ed94e2d2de209843ebc882 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_77a879c730c26ac9c89f5afbf4ef8a58f016682527ed94e2d2de209843ebc882->enter($__internal_77a879c730c26ac9c89f5afbf4ef8a58f016682527ed94e2d2de209843ebc882_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_bc7b5128bee3a29a1e3fd38585bab44fc6f911f036067bfaeefe27efe5fcca26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc7b5128bee3a29a1e3fd38585bab44fc6f911f036067bfaeefe27efe5fcca26->enter($__internal_bc7b5128bee3a29a1e3fd38585bab44fc6f911f036067bfaeefe27efe5fcca26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_bc7b5128bee3a29a1e3fd38585bab44fc6f911f036067bfaeefe27efe5fcca26->leave($__internal_bc7b5128bee3a29a1e3fd38585bab44fc6f911f036067bfaeefe27efe5fcca26_prof);

        
        $__internal_77a879c730c26ac9c89f5afbf4ef8a58f016682527ed94e2d2de209843ebc882->leave($__internal_77a879c730c26ac9c89f5afbf4ef8a58f016682527ed94e2d2de209843ebc882_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
