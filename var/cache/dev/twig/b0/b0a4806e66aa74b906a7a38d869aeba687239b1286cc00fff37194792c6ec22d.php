<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_89612bd6c62318aa241fa727dd099b5497e535db6ead2ad52446c47f14020461 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd09b62d6b24c925a733b47b21137f76fa661532a8056f2a41a49abfff054dd1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd09b62d6b24c925a733b47b21137f76fa661532a8056f2a41a49abfff054dd1->enter($__internal_fd09b62d6b24c925a733b47b21137f76fa661532a8056f2a41a49abfff054dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_bc4023a985d4583c61b61ad15b95bb460d187eec7885512e03f40361fd5af659 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc4023a985d4583c61b61ad15b95bb460d187eec7885512e03f40361fd5af659->enter($__internal_bc4023a985d4583c61b61ad15b95bb460d187eec7885512e03f40361fd5af659_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_fd09b62d6b24c925a733b47b21137f76fa661532a8056f2a41a49abfff054dd1->leave($__internal_fd09b62d6b24c925a733b47b21137f76fa661532a8056f2a41a49abfff054dd1_prof);

        
        $__internal_bc4023a985d4583c61b61ad15b95bb460d187eec7885512e03f40361fd5af659->leave($__internal_bc4023a985d4583c61b61ad15b95bb460d187eec7885512e03f40361fd5af659_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
