<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_010742bacd7922f8c0ef3868b50c1fd85132c12a1b910d45f0522f422e756bf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c676ba21e685699e9160c0b585db3071447531f16d8dd8ae6b99a56322ceb76 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c676ba21e685699e9160c0b585db3071447531f16d8dd8ae6b99a56322ceb76->enter($__internal_0c676ba21e685699e9160c0b585db3071447531f16d8dd8ae6b99a56322ceb76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_38a725f807c3efa2b991ac521598fcf6c6379cb112425570995867c167301168 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38a725f807c3efa2b991ac521598fcf6c6379cb112425570995867c167301168->enter($__internal_38a725f807c3efa2b991ac521598fcf6c6379cb112425570995867c167301168_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0c676ba21e685699e9160c0b585db3071447531f16d8dd8ae6b99a56322ceb76->leave($__internal_0c676ba21e685699e9160c0b585db3071447531f16d8dd8ae6b99a56322ceb76_prof);

        
        $__internal_38a725f807c3efa2b991ac521598fcf6c6379cb112425570995867c167301168->leave($__internal_38a725f807c3efa2b991ac521598fcf6c6379cb112425570995867c167301168_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_e585c7d9003d6cbf4075ac50c40f2b46d0cbb613fd902915bcea1621cf0d6d9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e585c7d9003d6cbf4075ac50c40f2b46d0cbb613fd902915bcea1621cf0d6d9f->enter($__internal_e585c7d9003d6cbf4075ac50c40f2b46d0cbb613fd902915bcea1621cf0d6d9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_109991d5a50121a02668584ac4b9851e7f4f8f722a799ab234b562c753b4f28a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_109991d5a50121a02668584ac4b9851e7f4f8f722a799ab234b562c753b4f28a->enter($__internal_109991d5a50121a02668584ac4b9851e7f4f8f722a799ab234b562c753b4f28a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_109991d5a50121a02668584ac4b9851e7f4f8f722a799ab234b562c753b4f28a->leave($__internal_109991d5a50121a02668584ac4b9851e7f4f8f722a799ab234b562c753b4f28a_prof);

        
        $__internal_e585c7d9003d6cbf4075ac50c40f2b46d0cbb613fd902915bcea1621cf0d6d9f->leave($__internal_e585c7d9003d6cbf4075ac50c40f2b46d0cbb613fd902915bcea1621cf0d6d9f_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7680dee77cda47ee3d34451da3e0b1e783b64c90f5f8e0f3f902700a17ada187 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7680dee77cda47ee3d34451da3e0b1e783b64c90f5f8e0f3f902700a17ada187->enter($__internal_7680dee77cda47ee3d34451da3e0b1e783b64c90f5f8e0f3f902700a17ada187_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_8e22e9d0b449a3c3a389bb6ce601f8626750999e04d4be5be729b46f495bb484 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e22e9d0b449a3c3a389bb6ce601f8626750999e04d4be5be729b46f495bb484->enter($__internal_8e22e9d0b449a3c3a389bb6ce601f8626750999e04d4be5be729b46f495bb484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_8e22e9d0b449a3c3a389bb6ce601f8626750999e04d4be5be729b46f495bb484->leave($__internal_8e22e9d0b449a3c3a389bb6ce601f8626750999e04d4be5be729b46f495bb484_prof);

        
        $__internal_7680dee77cda47ee3d34451da3e0b1e783b64c90f5f8e0f3f902700a17ada187->leave($__internal_7680dee77cda47ee3d34451da3e0b1e783b64c90f5f8e0f3f902700a17ada187_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_559316ff6638b9da8afb21ff92dcae1b14d561ae07a821399d355138bd813a96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_559316ff6638b9da8afb21ff92dcae1b14d561ae07a821399d355138bd813a96->enter($__internal_559316ff6638b9da8afb21ff92dcae1b14d561ae07a821399d355138bd813a96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c785101acea6f3cc3e2b73af19635d9156e7e46b84b289bd8e6cf5953561fdcd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c785101acea6f3cc3e2b73af19635d9156e7e46b84b289bd8e6cf5953561fdcd->enter($__internal_c785101acea6f3cc3e2b73af19635d9156e7e46b84b289bd8e6cf5953561fdcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_c785101acea6f3cc3e2b73af19635d9156e7e46b84b289bd8e6cf5953561fdcd->leave($__internal_c785101acea6f3cc3e2b73af19635d9156e7e46b84b289bd8e6cf5953561fdcd_prof);

        
        $__internal_559316ff6638b9da8afb21ff92dcae1b14d561ae07a821399d355138bd813a96->leave($__internal_559316ff6638b9da8afb21ff92dcae1b14d561ae07a821399d355138bd813a96_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
