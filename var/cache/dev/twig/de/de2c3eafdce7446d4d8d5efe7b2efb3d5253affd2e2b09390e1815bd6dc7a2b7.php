<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_badd646d4422676ef897103c4f6f755caf0e7ee2c31bf323e47191483cf58dc4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d967d32aa160be6b414dbbdca1a098e642628dd9e7180d7cfdba9a4d69451e14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d967d32aa160be6b414dbbdca1a098e642628dd9e7180d7cfdba9a4d69451e14->enter($__internal_d967d32aa160be6b414dbbdca1a098e642628dd9e7180d7cfdba9a4d69451e14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_4e5c14c784da2acf9d890249928248c41e03834cdc91b4682e93bedbaad211c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e5c14c784da2acf9d890249928248c41e03834cdc91b4682e93bedbaad211c5->enter($__internal_4e5c14c784da2acf9d890249928248c41e03834cdc91b4682e93bedbaad211c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_d967d32aa160be6b414dbbdca1a098e642628dd9e7180d7cfdba9a4d69451e14->leave($__internal_d967d32aa160be6b414dbbdca1a098e642628dd9e7180d7cfdba9a4d69451e14_prof);

        
        $__internal_4e5c14c784da2acf9d890249928248c41e03834cdc91b4682e93bedbaad211c5->leave($__internal_4e5c14c784da2acf9d890249928248c41e03834cdc91b4682e93bedbaad211c5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
