<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_6105d531764a47d2abf3d1eb9d52412c37a13bb3b8fe8d06ff3ce497d86df64b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a83bd944b8cdb4a3b30a9d45ae5fc6c471d70f041fc86ea6e0bfe25a168538a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a83bd944b8cdb4a3b30a9d45ae5fc6c471d70f041fc86ea6e0bfe25a168538a->enter($__internal_5a83bd944b8cdb4a3b30a9d45ae5fc6c471d70f041fc86ea6e0bfe25a168538a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_9ec51f9203fc00bb1a0813ff39e2bf66328b2a1302d18444592b5c8af78e6af4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ec51f9203fc00bb1a0813ff39e2bf66328b2a1302d18444592b5c8af78e6af4->enter($__internal_9ec51f9203fc00bb1a0813ff39e2bf66328b2a1302d18444592b5c8af78e6af4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_5a83bd944b8cdb4a3b30a9d45ae5fc6c471d70f041fc86ea6e0bfe25a168538a->leave($__internal_5a83bd944b8cdb4a3b30a9d45ae5fc6c471d70f041fc86ea6e0bfe25a168538a_prof);

        
        $__internal_9ec51f9203fc00bb1a0813ff39e2bf66328b2a1302d18444592b5c8af78e6af4->leave($__internal_9ec51f9203fc00bb1a0813ff39e2bf66328b2a1302d18444592b5c8af78e6af4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_container_attributes.html.php");
    }
}
