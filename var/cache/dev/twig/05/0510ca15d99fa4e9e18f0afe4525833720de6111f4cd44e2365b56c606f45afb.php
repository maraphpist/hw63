<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_2513e19d2d13d7bbf5d6f912b3544f49838f36e56415939132eddc3ef29bc2ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de647be44f7d8c63ffa0babab12fa8f64750bd9ddbd7a1d9d5dae98ce0a7ac4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de647be44f7d8c63ffa0babab12fa8f64750bd9ddbd7a1d9d5dae98ce0a7ac4d->enter($__internal_de647be44f7d8c63ffa0babab12fa8f64750bd9ddbd7a1d9d5dae98ce0a7ac4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_958b7ee6c32e3a6a204208d8eee74e45995711b531aa394de0f10db865a9e75a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_958b7ee6c32e3a6a204208d8eee74e45995711b531aa394de0f10db865a9e75a->enter($__internal_958b7ee6c32e3a6a204208d8eee74e45995711b531aa394de0f10db865a9e75a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_de647be44f7d8c63ffa0babab12fa8f64750bd9ddbd7a1d9d5dae98ce0a7ac4d->leave($__internal_de647be44f7d8c63ffa0babab12fa8f64750bd9ddbd7a1d9d5dae98ce0a7ac4d_prof);

        
        $__internal_958b7ee6c32e3a6a204208d8eee74e45995711b531aa394de0f10db865a9e75a->leave($__internal_958b7ee6c32e3a6a204208d8eee74e45995711b531aa394de0f10db865a9e75a_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_0c8f31fdc25497d8de51ec580a98a65e9ae05b9bc277df833234834dc3c04239 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c8f31fdc25497d8de51ec580a98a65e9ae05b9bc277df833234834dc3c04239->enter($__internal_0c8f31fdc25497d8de51ec580a98a65e9ae05b9bc277df833234834dc3c04239_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_774c9e1f20b5283e5c4ac01c1ff2af178de2d3f3e7a9b756b6e258c67afd152f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_774c9e1f20b5283e5c4ac01c1ff2af178de2d3f3e7a9b756b6e258c67afd152f->enter($__internal_774c9e1f20b5283e5c4ac01c1ff2af178de2d3f3e7a9b756b6e258c67afd152f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_774c9e1f20b5283e5c4ac01c1ff2af178de2d3f3e7a9b756b6e258c67afd152f->leave($__internal_774c9e1f20b5283e5c4ac01c1ff2af178de2d3f3e7a9b756b6e258c67afd152f_prof);

        
        $__internal_0c8f31fdc25497d8de51ec580a98a65e9ae05b9bc277df833234834dc3c04239->leave($__internal_0c8f31fdc25497d8de51ec580a98a65e9ae05b9bc277df833234834dc3c04239_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_2c2c80e3d2f1adefa9cc5366073ea592bd008a6c4eef19f09469c88bb45dd429 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c2c80e3d2f1adefa9cc5366073ea592bd008a6c4eef19f09469c88bb45dd429->enter($__internal_2c2c80e3d2f1adefa9cc5366073ea592bd008a6c4eef19f09469c88bb45dd429_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_d053f76bfb4ffc3b8995c6c70c811bf68554dba573283a5211f5dc41a990b7ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d053f76bfb4ffc3b8995c6c70c811bf68554dba573283a5211f5dc41a990b7ec->enter($__internal_d053f76bfb4ffc3b8995c6c70c811bf68554dba573283a5211f5dc41a990b7ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_d053f76bfb4ffc3b8995c6c70c811bf68554dba573283a5211f5dc41a990b7ec->leave($__internal_d053f76bfb4ffc3b8995c6c70c811bf68554dba573283a5211f5dc41a990b7ec_prof);

        
        $__internal_2c2c80e3d2f1adefa9cc5366073ea592bd008a6c4eef19f09469c88bb45dd429->leave($__internal_2c2c80e3d2f1adefa9cc5366073ea592bd008a6c4eef19f09469c88bb45dd429_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_c0a08d9cf2f884a3971e289b28c2af2b8c90e937ce3b5e230eb22180bfff296b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c0a08d9cf2f884a3971e289b28c2af2b8c90e937ce3b5e230eb22180bfff296b->enter($__internal_c0a08d9cf2f884a3971e289b28c2af2b8c90e937ce3b5e230eb22180bfff296b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_1b3280da25f6bb9b15311cdf86b0460c5b9597aac39ba6d9cf9d426f4fc4c492 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b3280da25f6bb9b15311cdf86b0460c5b9597aac39ba6d9cf9d426f4fc4c492->enter($__internal_1b3280da25f6bb9b15311cdf86b0460c5b9597aac39ba6d9cf9d426f4fc4c492_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_1b3280da25f6bb9b15311cdf86b0460c5b9597aac39ba6d9cf9d426f4fc4c492->leave($__internal_1b3280da25f6bb9b15311cdf86b0460c5b9597aac39ba6d9cf9d426f4fc4c492_prof);

        
        $__internal_c0a08d9cf2f884a3971e289b28c2af2b8c90e937ce3b5e230eb22180bfff296b->leave($__internal_c0a08d9cf2f884a3971e289b28c2af2b8c90e937ce3b5e230eb22180bfff296b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
