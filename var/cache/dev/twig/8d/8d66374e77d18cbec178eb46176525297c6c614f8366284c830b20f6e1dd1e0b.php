<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_62ace0ccecacfdc7c1b686bc690b2519e2504a94b53e1e84759afb9c2465d7d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_782c68e8f6f8edd2f64c4693ce93dbd35c83bdb780f6ef2f0656c506a4573d26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_782c68e8f6f8edd2f64c4693ce93dbd35c83bdb780f6ef2f0656c506a4573d26->enter($__internal_782c68e8f6f8edd2f64c4693ce93dbd35c83bdb780f6ef2f0656c506a4573d26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_15dfd14f4b638280f95cce0ffbca5e1bd1e71f37c7d01a27e1e4e1dc4ca56250 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15dfd14f4b638280f95cce0ffbca5e1bd1e71f37c7d01a27e1e4e1dc4ca56250->enter($__internal_15dfd14f4b638280f95cce0ffbca5e1bd1e71f37c7d01a27e1e4e1dc4ca56250_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_782c68e8f6f8edd2f64c4693ce93dbd35c83bdb780f6ef2f0656c506a4573d26->leave($__internal_782c68e8f6f8edd2f64c4693ce93dbd35c83bdb780f6ef2f0656c506a4573d26_prof);

        
        $__internal_15dfd14f4b638280f95cce0ffbca5e1bd1e71f37c7d01a27e1e4e1dc4ca56250->leave($__internal_15dfd14f4b638280f95cce0ffbca5e1bd1e71f37c7d01a27e1e4e1dc4ca56250_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d16601d91e10d815ce55c05c76a44c5526c90f524bb5fe6e157556e5c81e2c0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d16601d91e10d815ce55c05c76a44c5526c90f524bb5fe6e157556e5c81e2c0c->enter($__internal_d16601d91e10d815ce55c05c76a44c5526c90f524bb5fe6e157556e5c81e2c0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_78977b1b8611687962c9a5cc031528c99b31d3f572dee526b6ba09a280d4095b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78977b1b8611687962c9a5cc031528c99b31d3f572dee526b6ba09a280d4095b->enter($__internal_78977b1b8611687962c9a5cc031528c99b31d3f572dee526b6ba09a280d4095b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_78977b1b8611687962c9a5cc031528c99b31d3f572dee526b6ba09a280d4095b->leave($__internal_78977b1b8611687962c9a5cc031528c99b31d3f572dee526b6ba09a280d4095b_prof);

        
        $__internal_d16601d91e10d815ce55c05c76a44c5526c90f524bb5fe6e157556e5c81e2c0c->leave($__internal_d16601d91e10d815ce55c05c76a44c5526c90f524bb5fe6e157556e5c81e2c0c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
