<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_acacba1794e10f6c9f681ada3edf5afef455597ac8d2cf1b0f35f2e08cb37226 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6fb145cd809d425d3f0b6be8456dda4a1fc02f652782ad100373a5e65f715f61 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fb145cd809d425d3f0b6be8456dda4a1fc02f652782ad100373a5e65f715f61->enter($__internal_6fb145cd809d425d3f0b6be8456dda4a1fc02f652782ad100373a5e65f715f61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_e2c64977e1c15cb64e66b86dc3eb4898a223abeba3c936ea1c21094937b09d01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2c64977e1c15cb64e66b86dc3eb4898a223abeba3c936ea1c21094937b09d01->enter($__internal_e2c64977e1c15cb64e66b86dc3eb4898a223abeba3c936ea1c21094937b09d01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_6fb145cd809d425d3f0b6be8456dda4a1fc02f652782ad100373a5e65f715f61->leave($__internal_6fb145cd809d425d3f0b6be8456dda4a1fc02f652782ad100373a5e65f715f61_prof);

        
        $__internal_e2c64977e1c15cb64e66b86dc3eb4898a223abeba3c936ea1c21094937b09d01->leave($__internal_e2c64977e1c15cb64e66b86dc3eb4898a223abeba3c936ea1c21094937b09d01_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
