<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_119c72e7dff84766de0fd2a32db5fd6c286207212ae9347f60b3d1e16520fd97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3e49ef464121ee0e3248a08a2f7239bc244d0404eff8a3977bb4e59feec7ffc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3e49ef464121ee0e3248a08a2f7239bc244d0404eff8a3977bb4e59feec7ffc->enter($__internal_a3e49ef464121ee0e3248a08a2f7239bc244d0404eff8a3977bb4e59feec7ffc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_529a16c35d64249a3a66e696783ff17bd5c3dbe575fcd78561469c747d8c397b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_529a16c35d64249a3a66e696783ff17bd5c3dbe575fcd78561469c747d8c397b->enter($__internal_529a16c35d64249a3a66e696783ff17bd5c3dbe575fcd78561469c747d8c397b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_a3e49ef464121ee0e3248a08a2f7239bc244d0404eff8a3977bb4e59feec7ffc->leave($__internal_a3e49ef464121ee0e3248a08a2f7239bc244d0404eff8a3977bb4e59feec7ffc_prof);

        
        $__internal_529a16c35d64249a3a66e696783ff17bd5c3dbe575fcd78561469c747d8c397b->leave($__internal_529a16c35d64249a3a66e696783ff17bd5c3dbe575fcd78561469c747d8c397b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
