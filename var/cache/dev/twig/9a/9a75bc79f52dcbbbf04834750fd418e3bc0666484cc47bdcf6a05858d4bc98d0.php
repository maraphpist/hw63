<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_9fe2e90eb41f38649dbaf0d214681ed23e2a8bf3b12ee0f2f5c08f3d5f04ba59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd03954a29506eda78699faf351a43669ced45ef7fcf34232ddde5386c8da682 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd03954a29506eda78699faf351a43669ced45ef7fcf34232ddde5386c8da682->enter($__internal_cd03954a29506eda78699faf351a43669ced45ef7fcf34232ddde5386c8da682_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_3a119fb06707a1fac778cf258c170571e4e9bb6a03a80b0e0aaffc8e6c5e0b07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a119fb06707a1fac778cf258c170571e4e9bb6a03a80b0e0aaffc8e6c5e0b07->enter($__internal_3a119fb06707a1fac778cf258c170571e4e9bb6a03a80b0e0aaffc8e6c5e0b07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_cd03954a29506eda78699faf351a43669ced45ef7fcf34232ddde5386c8da682->leave($__internal_cd03954a29506eda78699faf351a43669ced45ef7fcf34232ddde5386c8da682_prof);

        
        $__internal_3a119fb06707a1fac778cf258c170571e4e9bb6a03a80b0e0aaffc8e6c5e0b07->leave($__internal_3a119fb06707a1fac778cf258c170571e4e9bb6a03a80b0e0aaffc8e6c5e0b07_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
