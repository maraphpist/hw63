<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_0fc2ba81e0194392c15cf97444fce413a6ce66a050af6dd7499f3a078f28bb52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f84cca6c798dd2fa832910ad3a3adfd3bbacc8bb5e1cdec867fe38f965ce2bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f84cca6c798dd2fa832910ad3a3adfd3bbacc8bb5e1cdec867fe38f965ce2bd->enter($__internal_7f84cca6c798dd2fa832910ad3a3adfd3bbacc8bb5e1cdec867fe38f965ce2bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_107b883b7646db072803dd056ad2ef992623687d8caffaf313d8ffaa0642be9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_107b883b7646db072803dd056ad2ef992623687d8caffaf313d8ffaa0642be9f->enter($__internal_107b883b7646db072803dd056ad2ef992623687d8caffaf313d8ffaa0642be9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_7f84cca6c798dd2fa832910ad3a3adfd3bbacc8bb5e1cdec867fe38f965ce2bd->leave($__internal_7f84cca6c798dd2fa832910ad3a3adfd3bbacc8bb5e1cdec867fe38f965ce2bd_prof);

        
        $__internal_107b883b7646db072803dd056ad2ef992623687d8caffaf313d8ffaa0642be9f->leave($__internal_107b883b7646db072803dd056ad2ef992623687d8caffaf313d8ffaa0642be9f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_attributes.html.php");
    }
}
