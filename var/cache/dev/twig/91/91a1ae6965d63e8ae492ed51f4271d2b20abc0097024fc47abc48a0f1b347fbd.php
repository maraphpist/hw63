<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_cf5a95adbac54adef95483068906b27542e973e054111880720610620c75663d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_600b07e38b4fe4641db590c558cd58436c23069173ed65752efbe07e0d61b59e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_600b07e38b4fe4641db590c558cd58436c23069173ed65752efbe07e0d61b59e->enter($__internal_600b07e38b4fe4641db590c558cd58436c23069173ed65752efbe07e0d61b59e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_dd2ac59732107717de47b8e8d41ac7ab4af5a0522db8289ec344a8bf1cb17c81 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd2ac59732107717de47b8e8d41ac7ab4af5a0522db8289ec344a8bf1cb17c81->enter($__internal_dd2ac59732107717de47b8e8d41ac7ab4af5a0522db8289ec344a8bf1cb17c81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_600b07e38b4fe4641db590c558cd58436c23069173ed65752efbe07e0d61b59e->leave($__internal_600b07e38b4fe4641db590c558cd58436c23069173ed65752efbe07e0d61b59e_prof);

        
        $__internal_dd2ac59732107717de47b8e8d41ac7ab4af5a0522db8289ec344a8bf1cb17c81->leave($__internal_dd2ac59732107717de47b8e8d41ac7ab4af5a0522db8289ec344a8bf1cb17c81_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1cc1f4b2d63baf8a9c98a099a1a0927cc7de747c05adcca6f6e87319511824ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1cc1f4b2d63baf8a9c98a099a1a0927cc7de747c05adcca6f6e87319511824ed->enter($__internal_1cc1f4b2d63baf8a9c98a099a1a0927cc7de747c05adcca6f6e87319511824ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_84ac65730aa3acc055e6e0cbe36b1022816f34f18f232a3bd6e69edd35614410 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84ac65730aa3acc055e6e0cbe36b1022816f34f18f232a3bd6e69edd35614410->enter($__internal_84ac65730aa3acc055e6e0cbe36b1022816f34f18f232a3bd6e69edd35614410_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_84ac65730aa3acc055e6e0cbe36b1022816f34f18f232a3bd6e69edd35614410->leave($__internal_84ac65730aa3acc055e6e0cbe36b1022816f34f18f232a3bd6e69edd35614410_prof);

        
        $__internal_1cc1f4b2d63baf8a9c98a099a1a0927cc7de747c05adcca6f6e87319511824ed->leave($__internal_1cc1f4b2d63baf8a9c98a099a1a0927cc7de747c05adcca6f6e87319511824ed_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
