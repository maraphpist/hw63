<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_14ee7c3912401387de17c56ebfb16063f7708920acbbc5432689de80cfcc7df9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_855a45983388ab29764d800aa1e175864da775524028ed7682ede9733e01af15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_855a45983388ab29764d800aa1e175864da775524028ed7682ede9733e01af15->enter($__internal_855a45983388ab29764d800aa1e175864da775524028ed7682ede9733e01af15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_a9391d96cd94dd13dbd1e620c7947f45277f0bb5d6184c62de0701e93cbeff66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9391d96cd94dd13dbd1e620c7947f45277f0bb5d6184c62de0701e93cbeff66->enter($__internal_a9391d96cd94dd13dbd1e620c7947f45277f0bb5d6184c62de0701e93cbeff66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_855a45983388ab29764d800aa1e175864da775524028ed7682ede9733e01af15->leave($__internal_855a45983388ab29764d800aa1e175864da775524028ed7682ede9733e01af15_prof);

        
        $__internal_a9391d96cd94dd13dbd1e620c7947f45277f0bb5d6184c62de0701e93cbeff66->leave($__internal_a9391d96cd94dd13dbd1e620c7947f45277f0bb5d6184c62de0701e93cbeff66_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
