<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_6e976f8b65a81c21affc1ecaf9a24ba05f917b1e59d98ff62190a6231c84be59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_002ebd529c2b83b082b0c77988bce874e17c34d03e2e29f2b80fa51efde1855e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_002ebd529c2b83b082b0c77988bce874e17c34d03e2e29f2b80fa51efde1855e->enter($__internal_002ebd529c2b83b082b0c77988bce874e17c34d03e2e29f2b80fa51efde1855e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_33ebcd49565002dae2ecf0905d5b9b57a47bdb72412366c61a46aa044a46bc7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33ebcd49565002dae2ecf0905d5b9b57a47bdb72412366c61a46aa044a46bc7a->enter($__internal_33ebcd49565002dae2ecf0905d5b9b57a47bdb72412366c61a46aa044a46bc7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_002ebd529c2b83b082b0c77988bce874e17c34d03e2e29f2b80fa51efde1855e->leave($__internal_002ebd529c2b83b082b0c77988bce874e17c34d03e2e29f2b80fa51efde1855e_prof);

        
        $__internal_33ebcd49565002dae2ecf0905d5b9b57a47bdb72412366c61a46aa044a46bc7a->leave($__internal_33ebcd49565002dae2ecf0905d5b9b57a47bdb72412366c61a46aa044a46bc7a_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_550e3ec0cce319f8dba006139e2cea826cc39341154c856db7938a6e4d373293 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_550e3ec0cce319f8dba006139e2cea826cc39341154c856db7938a6e4d373293->enter($__internal_550e3ec0cce319f8dba006139e2cea826cc39341154c856db7938a6e4d373293_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d6467049bf02a787e6ca975c5af6b9a777b22c69a81f5b74a01aa4274b5e8680 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6467049bf02a787e6ca975c5af6b9a777b22c69a81f5b74a01aa4274b5e8680->enter($__internal_d6467049bf02a787e6ca975c5af6b9a777b22c69a81f5b74a01aa4274b5e8680_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_d6467049bf02a787e6ca975c5af6b9a777b22c69a81f5b74a01aa4274b5e8680->leave($__internal_d6467049bf02a787e6ca975c5af6b9a777b22c69a81f5b74a01aa4274b5e8680_prof);

        
        $__internal_550e3ec0cce319f8dba006139e2cea826cc39341154c856db7938a6e4d373293->leave($__internal_550e3ec0cce319f8dba006139e2cea826cc39341154c856db7938a6e4d373293_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
