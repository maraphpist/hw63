<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_bf1e9f33d8870d9f7194eb22f9bd0f79ed8c8337b2955d3761d55338cfa86369 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13fb65cc59ac0c795cb7b12b071fcc7db7410c17841446e8b1906d8307da365b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13fb65cc59ac0c795cb7b12b071fcc7db7410c17841446e8b1906d8307da365b->enter($__internal_13fb65cc59ac0c795cb7b12b071fcc7db7410c17841446e8b1906d8307da365b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_94c07e51fe87f882d5c5c2cc75fbd0664254ca87ff0af374e19213087a439a13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94c07e51fe87f882d5c5c2cc75fbd0664254ca87ff0af374e19213087a439a13->enter($__internal_94c07e51fe87f882d5c5c2cc75fbd0664254ca87ff0af374e19213087a439a13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_13fb65cc59ac0c795cb7b12b071fcc7db7410c17841446e8b1906d8307da365b->leave($__internal_13fb65cc59ac0c795cb7b12b071fcc7db7410c17841446e8b1906d8307da365b_prof);

        
        $__internal_94c07e51fe87f882d5c5c2cc75fbd0664254ca87ff0af374e19213087a439a13->leave($__internal_94c07e51fe87f882d5c5c2cc75fbd0664254ca87ff0af374e19213087a439a13_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
