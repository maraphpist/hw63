<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_6be2fa8c026f231cc0acc0b18d7daeaf8f7b74e8b59c58c4285bb6535f71616a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_393d82db26811fc0ed35bd63a910cc88b402da6ce555e724cd4bfe413c3c3bef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_393d82db26811fc0ed35bd63a910cc88b402da6ce555e724cd4bfe413c3c3bef->enter($__internal_393d82db26811fc0ed35bd63a910cc88b402da6ce555e724cd4bfe413c3c3bef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_5dc912f6a9c421572a548ed0138e54696eda4ee54b1a6eaec6b6d6e69ba56a43 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5dc912f6a9c421572a548ed0138e54696eda4ee54b1a6eaec6b6d6e69ba56a43->enter($__internal_5dc912f6a9c421572a548ed0138e54696eda4ee54b1a6eaec6b6d6e69ba56a43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_393d82db26811fc0ed35bd63a910cc88b402da6ce555e724cd4bfe413c3c3bef->leave($__internal_393d82db26811fc0ed35bd63a910cc88b402da6ce555e724cd4bfe413c3c3bef_prof);

        
        $__internal_5dc912f6a9c421572a548ed0138e54696eda4ee54b1a6eaec6b6d6e69ba56a43->leave($__internal_5dc912f6a9c421572a548ed0138e54696eda4ee54b1a6eaec6b6d6e69ba56a43_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_4df50a6ce5b15303b3c69a78fda15612b34284398c2616d385894b71b5f1bebd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4df50a6ce5b15303b3c69a78fda15612b34284398c2616d385894b71b5f1bebd->enter($__internal_4df50a6ce5b15303b3c69a78fda15612b34284398c2616d385894b71b5f1bebd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_a2ccfe0647197d9281d7c67f00dc13ee5c52fa6f69004ea646110055b2560255 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2ccfe0647197d9281d7c67f00dc13ee5c52fa6f69004ea646110055b2560255->enter($__internal_a2ccfe0647197d9281d7c67f00dc13ee5c52fa6f69004ea646110055b2560255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_a2ccfe0647197d9281d7c67f00dc13ee5c52fa6f69004ea646110055b2560255->leave($__internal_a2ccfe0647197d9281d7c67f00dc13ee5c52fa6f69004ea646110055b2560255_prof);

        
        $__internal_4df50a6ce5b15303b3c69a78fda15612b34284398c2616d385894b71b5f1bebd->leave($__internal_4df50a6ce5b15303b3c69a78fda15612b34284398c2616d385894b71b5f1bebd_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_f135d9ac62a80d75ccbe1ba4833da3859f64dd51da9efe8a744cceb35c3fd10a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f135d9ac62a80d75ccbe1ba4833da3859f64dd51da9efe8a744cceb35c3fd10a->enter($__internal_f135d9ac62a80d75ccbe1ba4833da3859f64dd51da9efe8a744cceb35c3fd10a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_5951daeaff58586934740759c0c9938de29e3aadd8b0f5ddb0fa4d0c8fbfe5d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5951daeaff58586934740759c0c9938de29e3aadd8b0f5ddb0fa4d0c8fbfe5d5->enter($__internal_5951daeaff58586934740759c0c9938de29e3aadd8b0f5ddb0fa4d0c8fbfe5d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_5951daeaff58586934740759c0c9938de29e3aadd8b0f5ddb0fa4d0c8fbfe5d5->leave($__internal_5951daeaff58586934740759c0c9938de29e3aadd8b0f5ddb0fa4d0c8fbfe5d5_prof);

        
        $__internal_f135d9ac62a80d75ccbe1ba4833da3859f64dd51da9efe8a744cceb35c3fd10a->leave($__internal_f135d9ac62a80d75ccbe1ba4833da3859f64dd51da9efe8a744cceb35c3fd10a_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_cda5163659c10fafe8b55d2aba928868b8142405484f740a5613485390857188 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cda5163659c10fafe8b55d2aba928868b8142405484f740a5613485390857188->enter($__internal_cda5163659c10fafe8b55d2aba928868b8142405484f740a5613485390857188_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_886593bda29c44833d2149c5a1d671910c85267e5bafbd46bf0bbcc1a919cfa6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_886593bda29c44833d2149c5a1d671910c85267e5bafbd46bf0bbcc1a919cfa6->enter($__internal_886593bda29c44833d2149c5a1d671910c85267e5bafbd46bf0bbcc1a919cfa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_886593bda29c44833d2149c5a1d671910c85267e5bafbd46bf0bbcc1a919cfa6->leave($__internal_886593bda29c44833d2149c5a1d671910c85267e5bafbd46bf0bbcc1a919cfa6_prof);

        
        $__internal_cda5163659c10fafe8b55d2aba928868b8142405484f740a5613485390857188->leave($__internal_cda5163659c10fafe8b55d2aba928868b8142405484f740a5613485390857188_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
