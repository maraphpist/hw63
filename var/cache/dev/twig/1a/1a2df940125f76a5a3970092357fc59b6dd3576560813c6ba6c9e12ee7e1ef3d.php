<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_0da1c25ba5562351c3a054bfd1e40c9ce90e383bc266e3837526ed18557ab818 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc1ba11c83661577bb1403af56e09010a5cb21b565e7b996a989ebc147de28e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc1ba11c83661577bb1403af56e09010a5cb21b565e7b996a989ebc147de28e2->enter($__internal_cc1ba11c83661577bb1403af56e09010a5cb21b565e7b996a989ebc147de28e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_3a809e371ef15f94af3091b7594aadc5973cda2f2377121f9589dc6b760a07fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a809e371ef15f94af3091b7594aadc5973cda2f2377121f9589dc6b760a07fd->enter($__internal_3a809e371ef15f94af3091b7594aadc5973cda2f2377121f9589dc6b760a07fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_cc1ba11c83661577bb1403af56e09010a5cb21b565e7b996a989ebc147de28e2->leave($__internal_cc1ba11c83661577bb1403af56e09010a5cb21b565e7b996a989ebc147de28e2_prof);

        
        $__internal_3a809e371ef15f94af3091b7594aadc5973cda2f2377121f9589dc6b760a07fd->leave($__internal_3a809e371ef15f94af3091b7594aadc5973cda2f2377121f9589dc6b760a07fd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
