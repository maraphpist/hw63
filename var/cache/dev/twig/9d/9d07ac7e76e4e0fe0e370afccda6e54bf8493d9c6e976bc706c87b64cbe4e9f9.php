<?php

/* @WebProfiler/Icon/search.svg */
class __TwigTemplate_90a8deb55108b1db8c10bfeb0e805252c84710fd0148905e612799e3c28984a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba28a5ad913901fd6ee39b89362f934cd40938a24742d7322f49bebcff66cdba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba28a5ad913901fd6ee39b89362f934cd40938a24742d7322f49bebcff66cdba->enter($__internal_ba28a5ad913901fd6ee39b89362f934cd40938a24742d7322f49bebcff66cdba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/search.svg"));

        $__internal_3a8c34d0a60389d2597c5298384c0da1579ebf35231df96719737f1a6c8e328a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a8c34d0a60389d2597c5298384c0da1579ebf35231df96719737f1a6c8e328a->enter($__internal_3a8c34d0a60389d2597c5298384c0da1579ebf35231df96719737f1a6c8e328a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/search.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M11.61,0.357c-4.4,0-7.98,3.58-7.98,7.98c0,1.696,0.526,3.308,1.524,4.679l-4.374,4.477
        c-0.238,0.238-0.369,0.554-0.369,0.891c0,0.336,0.131,0.653,0.369,0.891c0.238,0.238,0.554,0.369,0.891,0.369
        c0.336,0,0.653-0.131,0.893-0.371l4.372-4.475c1.369,0.996,2.98,1.521,4.674,1.521c4.4,0,7.98-3.58,7.98-7.98
        S16.01,0.357,11.61,0.357z M17.07,8.337c0,3.011-2.449,5.46-5.46,5.46c-3.011,0-5.46-2.449-5.46-5.46s2.449-5.46,5.46-5.46
        C14.62,2.877,17.07,5.326,17.07,8.337z\"/>
</svg>
";
        
        $__internal_ba28a5ad913901fd6ee39b89362f934cd40938a24742d7322f49bebcff66cdba->leave($__internal_ba28a5ad913901fd6ee39b89362f934cd40938a24742d7322f49bebcff66cdba_prof);

        
        $__internal_3a8c34d0a60389d2597c5298384c0da1579ebf35231df96719737f1a6c8e328a->leave($__internal_3a8c34d0a60389d2597c5298384c0da1579ebf35231df96719737f1a6c8e328a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/search.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M11.61,0.357c-4.4,0-7.98,3.58-7.98,7.98c0,1.696,0.526,3.308,1.524,4.679l-4.374,4.477
        c-0.238,0.238-0.369,0.554-0.369,0.891c0,0.336,0.131,0.653,0.369,0.891c0.238,0.238,0.554,0.369,0.891,0.369
        c0.336,0,0.653-0.131,0.893-0.371l4.372-4.475c1.369,0.996,2.98,1.521,4.674,1.521c4.4,0,7.98-3.58,7.98-7.98
        S16.01,0.357,11.61,0.357z M17.07,8.337c0,3.011-2.449,5.46-5.46,5.46c-3.011,0-5.46-2.449-5.46-5.46s2.449-5.46,5.46-5.46
        C14.62,2.877,17.07,5.326,17.07,8.337z\"/>
</svg>
", "@WebProfiler/Icon/search.svg", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/search.svg");
    }
}
