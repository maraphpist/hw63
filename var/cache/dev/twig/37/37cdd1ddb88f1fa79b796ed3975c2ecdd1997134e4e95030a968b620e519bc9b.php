<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_4ecdb2e19f0086f9f6b489f07be0e8c037d159b1ddf0fb84424914e8d5579502 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9fb2703af3ead53c1482235be67ac5f0593d5503689b8a87f6072655f2f21512 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9fb2703af3ead53c1482235be67ac5f0593d5503689b8a87f6072655f2f21512->enter($__internal_9fb2703af3ead53c1482235be67ac5f0593d5503689b8a87f6072655f2f21512_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_d15ded7ba2ca9e8d42e2ffcb425c64f9f5b55236d9ebdd537a19ed7546ca75d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d15ded7ba2ca9e8d42e2ffcb425c64f9f5b55236d9ebdd537a19ed7546ca75d0->enter($__internal_d15ded7ba2ca9e8d42e2ffcb425c64f9f5b55236d9ebdd537a19ed7546ca75d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9fb2703af3ead53c1482235be67ac5f0593d5503689b8a87f6072655f2f21512->leave($__internal_9fb2703af3ead53c1482235be67ac5f0593d5503689b8a87f6072655f2f21512_prof);

        
        $__internal_d15ded7ba2ca9e8d42e2ffcb425c64f9f5b55236d9ebdd537a19ed7546ca75d0->leave($__internal_d15ded7ba2ca9e8d42e2ffcb425c64f9f5b55236d9ebdd537a19ed7546ca75d0_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3b62ba52c620fb78fe2dbdd9cb0bef67f22afb38315d22c842b6da8024e5f5b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b62ba52c620fb78fe2dbdd9cb0bef67f22afb38315d22c842b6da8024e5f5b1->enter($__internal_3b62ba52c620fb78fe2dbdd9cb0bef67f22afb38315d22c842b6da8024e5f5b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e02a48ed582fde16c1b298014f3d3f1374c442e6cb0314113f2c177211771a9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e02a48ed582fde16c1b298014f3d3f1374c442e6cb0314113f2c177211771a9c->enter($__internal_e02a48ed582fde16c1b298014f3d3f1374c442e6cb0314113f2c177211771a9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_e02a48ed582fde16c1b298014f3d3f1374c442e6cb0314113f2c177211771a9c->leave($__internal_e02a48ed582fde16c1b298014f3d3f1374c442e6cb0314113f2c177211771a9c_prof);

        
        $__internal_3b62ba52c620fb78fe2dbdd9cb0bef67f22afb38315d22c842b6da8024e5f5b1->leave($__internal_3b62ba52c620fb78fe2dbdd9cb0bef67f22afb38315d22c842b6da8024e5f5b1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
