<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_9ec924366e2f87664ba94044a4897bf66d2b63a663b41eee1f997752f7076340 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cda86e051da3d1771b966c4ccaccb4c6dcd18f7b60e615325f27fb360bbb3534 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cda86e051da3d1771b966c4ccaccb4c6dcd18f7b60e615325f27fb360bbb3534->enter($__internal_cda86e051da3d1771b966c4ccaccb4c6dcd18f7b60e615325f27fb360bbb3534_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_58d98e4dae17d0dd570cb0be17062d2a408e226edd03510d077d7406e4cfd8c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58d98e4dae17d0dd570cb0be17062d2a408e226edd03510d077d7406e4cfd8c9->enter($__internal_58d98e4dae17d0dd570cb0be17062d2a408e226edd03510d077d7406e4cfd8c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_cda86e051da3d1771b966c4ccaccb4c6dcd18f7b60e615325f27fb360bbb3534->leave($__internal_cda86e051da3d1771b966c4ccaccb4c6dcd18f7b60e615325f27fb360bbb3534_prof);

        
        $__internal_58d98e4dae17d0dd570cb0be17062d2a408e226edd03510d077d7406e4cfd8c9->leave($__internal_58d98e4dae17d0dd570cb0be17062d2a408e226edd03510d077d7406e4cfd8c9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
