<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_d4bbefb3b0dd532cc5b5f0a8b92e321fb27daac20e573b0a169576677f285fe4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44c58cddb156a85213790311ac32a2729b4b9d74678b0fd540e9c793bda2bd4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44c58cddb156a85213790311ac32a2729b4b9d74678b0fd540e9c793bda2bd4a->enter($__internal_44c58cddb156a85213790311ac32a2729b4b9d74678b0fd540e9c793bda2bd4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_7fc614a02d7dc886e57773de32abf5325e3d567f90be7247e38eb94387c650ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7fc614a02d7dc886e57773de32abf5325e3d567f90be7247e38eb94387c650ce->enter($__internal_7fc614a02d7dc886e57773de32abf5325e3d567f90be7247e38eb94387c650ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_44c58cddb156a85213790311ac32a2729b4b9d74678b0fd540e9c793bda2bd4a->leave($__internal_44c58cddb156a85213790311ac32a2729b4b9d74678b0fd540e9c793bda2bd4a_prof);

        
        $__internal_7fc614a02d7dc886e57773de32abf5325e3d567f90be7247e38eb94387c650ce->leave($__internal_7fc614a02d7dc886e57773de32abf5325e3d567f90be7247e38eb94387c650ce_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
