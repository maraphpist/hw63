<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_565d7b34017e7b0d00cabe1ad15d79198e7f69bc178b026aab416bff8ead0d57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0ae38680b3daced78be447c5aa6d9d6cde050a767e83545ac295efb16112aa8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0ae38680b3daced78be447c5aa6d9d6cde050a767e83545ac295efb16112aa8->enter($__internal_a0ae38680b3daced78be447c5aa6d9d6cde050a767e83545ac295efb16112aa8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_df1fd6ae6ee5ffd233f8e272b4f301fef7fe4a8fac8ba442f661f7019fd6911c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df1fd6ae6ee5ffd233f8e272b4f301fef7fe4a8fac8ba442f661f7019fd6911c->enter($__internal_df1fd6ae6ee5ffd233f8e272b4f301fef7fe4a8fac8ba442f661f7019fd6911c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_a0ae38680b3daced78be447c5aa6d9d6cde050a767e83545ac295efb16112aa8->leave($__internal_a0ae38680b3daced78be447c5aa6d9d6cde050a767e83545ac295efb16112aa8_prof);

        
        $__internal_df1fd6ae6ee5ffd233f8e272b4f301fef7fe4a8fac8ba442f661f7019fd6911c->leave($__internal_df1fd6ae6ee5ffd233f8e272b4f301fef7fe4a8fac8ba442f661f7019fd6911c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
