<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_8f7e9f0fe9a7c66fc55fbb3b07c90a445a4bab8522dd1c59526c3029735b93e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f792bddbf039590f196fb9bfdd42a010ce944e13c6fc3ecd809a569488cffb7b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f792bddbf039590f196fb9bfdd42a010ce944e13c6fc3ecd809a569488cffb7b->enter($__internal_f792bddbf039590f196fb9bfdd42a010ce944e13c6fc3ecd809a569488cffb7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_a1a8c197fcedc9a78b48869e9d37a67f198775157fef3d20be20fb464f8dd089 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1a8c197fcedc9a78b48869e9d37a67f198775157fef3d20be20fb464f8dd089->enter($__internal_a1a8c197fcedc9a78b48869e9d37a67f198775157fef3d20be20fb464f8dd089_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_f792bddbf039590f196fb9bfdd42a010ce944e13c6fc3ecd809a569488cffb7b->leave($__internal_f792bddbf039590f196fb9bfdd42a010ce944e13c6fc3ecd809a569488cffb7b_prof);

        
        $__internal_a1a8c197fcedc9a78b48869e9d37a67f198775157fef3d20be20fb464f8dd089->leave($__internal_a1a8c197fcedc9a78b48869e9d37a67f198775157fef3d20be20fb464f8dd089_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
