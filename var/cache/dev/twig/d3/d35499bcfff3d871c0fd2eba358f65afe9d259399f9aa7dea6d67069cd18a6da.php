<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_c3b7a7c8c14804fa82af7e9764f82f9533f1f0aab5d777390c7d2e1ea37812e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c319adbba23eacd7e692712e18d711e169720d3ee473ca1f856f4ea5ecdc922d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c319adbba23eacd7e692712e18d711e169720d3ee473ca1f856f4ea5ecdc922d->enter($__internal_c319adbba23eacd7e692712e18d711e169720d3ee473ca1f856f4ea5ecdc922d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $__internal_b5ac95b3da2b8b9724bea23557731092566fb7d118c659f58eea0d6b5ccd2fca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5ac95b3da2b8b9724bea23557731092566fb7d118c659f58eea0d6b5ccd2fca->enter($__internal_b5ac95b3da2b8b9724bea23557731092566fb7d118c659f58eea0d6b5ccd2fca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_c319adbba23eacd7e692712e18d711e169720d3ee473ca1f856f4ea5ecdc922d->leave($__internal_c319adbba23eacd7e692712e18d711e169720d3ee473ca1f856f4ea5ecdc922d_prof);

        
        $__internal_b5ac95b3da2b8b9724bea23557731092566fb7d118c659f58eea0d6b5ccd2fca->leave($__internal_b5ac95b3da2b8b9724bea23557731092566fb7d118c659f58eea0d6b5ccd2fca_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}
