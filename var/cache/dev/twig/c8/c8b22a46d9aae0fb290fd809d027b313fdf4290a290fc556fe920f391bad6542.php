<?php

/* ::base.html.twig */
class __TwigTemplate_47fd51b039e5f4fb7e336b9f260737cefed48cbce97d781ee1d74e5c39c4c062 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3bee6f10b984805652e52c6fad5424715c799de637375855e112cb9b71f3f909 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3bee6f10b984805652e52c6fad5424715c799de637375855e112cb9b71f3f909->enter($__internal_3bee6f10b984805652e52c6fad5424715c799de637375855e112cb9b71f3f909_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_c335594e53154cf2388c6ca9f6cfd6c84431d69d1b50155335328f126ba54981 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c335594e53154cf2388c6ca9f6cfd6c84431d69d1b50155335328f126ba54981->enter($__internal_c335594e53154cf2388c6ca9f6cfd6c84431d69d1b50155335328f126ba54981_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 13
        echo "    </body>
</html>
";
        
        $__internal_3bee6f10b984805652e52c6fad5424715c799de637375855e112cb9b71f3f909->leave($__internal_3bee6f10b984805652e52c6fad5424715c799de637375855e112cb9b71f3f909_prof);

        
        $__internal_c335594e53154cf2388c6ca9f6cfd6c84431d69d1b50155335328f126ba54981->leave($__internal_c335594e53154cf2388c6ca9f6cfd6c84431d69d1b50155335328f126ba54981_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_f82f8813c4e3f9cdcd79482872cf2afb9a9907fa745dd2390814fa11014332f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f82f8813c4e3f9cdcd79482872cf2afb9a9907fa745dd2390814fa11014332f1->enter($__internal_f82f8813c4e3f9cdcd79482872cf2afb9a9907fa745dd2390814fa11014332f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_afcc4cc0bc52f3159c82302aa4125f10366ae859d4d5503cad8c39939062c93f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afcc4cc0bc52f3159c82302aa4125f10366ae859d4d5503cad8c39939062c93f->enter($__internal_afcc4cc0bc52f3159c82302aa4125f10366ae859d4d5503cad8c39939062c93f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_afcc4cc0bc52f3159c82302aa4125f10366ae859d4d5503cad8c39939062c93f->leave($__internal_afcc4cc0bc52f3159c82302aa4125f10366ae859d4d5503cad8c39939062c93f_prof);

        
        $__internal_f82f8813c4e3f9cdcd79482872cf2afb9a9907fa745dd2390814fa11014332f1->leave($__internal_f82f8813c4e3f9cdcd79482872cf2afb9a9907fa745dd2390814fa11014332f1_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_49ac4bac208a628afa87d80c4e7f84a958ced2b3535beb08dee99bb168919cee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49ac4bac208a628afa87d80c4e7f84a958ced2b3535beb08dee99bb168919cee->enter($__internal_49ac4bac208a628afa87d80c4e7f84a958ced2b3535beb08dee99bb168919cee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_ee43bf9ae29d71dfae5323a9ecd075f2082f07b41938c47d3efd9a5f399734ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee43bf9ae29d71dfae5323a9ecd075f2082f07b41938c47d3efd9a5f399734ec->enter($__internal_ee43bf9ae29d71dfae5323a9ecd075f2082f07b41938c47d3efd9a5f399734ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_ee43bf9ae29d71dfae5323a9ecd075f2082f07b41938c47d3efd9a5f399734ec->leave($__internal_ee43bf9ae29d71dfae5323a9ecd075f2082f07b41938c47d3efd9a5f399734ec_prof);

        
        $__internal_49ac4bac208a628afa87d80c4e7f84a958ced2b3535beb08dee99bb168919cee->leave($__internal_49ac4bac208a628afa87d80c4e7f84a958ced2b3535beb08dee99bb168919cee_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_3bef557bdaeb754035cd328686c2f93db2dca466f31cce44d4112358d580cfd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3bef557bdaeb754035cd328686c2f93db2dca466f31cce44d4112358d580cfd5->enter($__internal_3bef557bdaeb754035cd328686c2f93db2dca466f31cce44d4112358d580cfd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e4883e33baf85d0be8c0bd16578f7a8052927cf818ae7b5b39d0aeb1546907d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4883e33baf85d0be8c0bd16578f7a8052927cf818ae7b5b39d0aeb1546907d9->enter($__internal_e4883e33baf85d0be8c0bd16578f7a8052927cf818ae7b5b39d0aeb1546907d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_e4883e33baf85d0be8c0bd16578f7a8052927cf818ae7b5b39d0aeb1546907d9->leave($__internal_e4883e33baf85d0be8c0bd16578f7a8052927cf818ae7b5b39d0aeb1546907d9_prof);

        
        $__internal_3bef557bdaeb754035cd328686c2f93db2dca466f31cce44d4112358d580cfd5->leave($__internal_3bef557bdaeb754035cd328686c2f93db2dca466f31cce44d4112358d580cfd5_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_699877213ba299579060684ded7f1825aaca0d7e7b23b1ebd03bfa4626972937 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_699877213ba299579060684ded7f1825aaca0d7e7b23b1ebd03bfa4626972937->enter($__internal_699877213ba299579060684ded7f1825aaca0d7e7b23b1ebd03bfa4626972937_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_64fe1bca9e5ec72ccc8883411903854ae3a21afa9a60fb56a76e122f7999c730 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64fe1bca9e5ec72ccc8883411903854ae3a21afa9a60fb56a76e122f7999c730->enter($__internal_64fe1bca9e5ec72ccc8883411903854ae3a21afa9a60fb56a76e122f7999c730_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        echo " ";
        
        $__internal_64fe1bca9e5ec72ccc8883411903854ae3a21afa9a60fb56a76e122f7999c730->leave($__internal_64fe1bca9e5ec72ccc8883411903854ae3a21afa9a60fb56a76e122f7999c730_prof);

        
        $__internal_699877213ba299579060684ded7f1825aaca0d7e7b23b1ebd03bfa4626972937->leave($__internal_699877213ba299579060684ded7f1825aaca0d7e7b23b1ebd03bfa4626972937_prof);

    }

    // line 12
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_19773619d2266103bade130797ee428fe4aa30f031eaa98b97299c8ee9d4b453 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19773619d2266103bade130797ee428fe4aa30f031eaa98b97299c8ee9d4b453->enter($__internal_19773619d2266103bade130797ee428fe4aa30f031eaa98b97299c8ee9d4b453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_1df2865535fd36866dae2750cb4d68f14d2900955cdd18bd33c5f632c9f497fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1df2865535fd36866dae2750cb4d68f14d2900955cdd18bd33c5f632c9f497fd->enter($__internal_1df2865535fd36866dae2750cb4d68f14d2900955cdd18bd33c5f632c9f497fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_1df2865535fd36866dae2750cb4d68f14d2900955cdd18bd33c5f632c9f497fd->leave($__internal_1df2865535fd36866dae2750cb4d68f14d2900955cdd18bd33c5f632c9f497fd_prof);

        
        $__internal_19773619d2266103bade130797ee428fe4aa30f031eaa98b97299c8ee9d4b453->leave($__internal_19773619d2266103bade130797ee428fe4aa30f031eaa98b97299c8ee9d4b453_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 12,  121 => 11,  104 => 10,  87 => 6,  69 => 5,  57 => 13,  54 => 12,  51 => 11,  49 => 10,  42 => 7,  40 => 6,  36 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block content %} {% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/var/www/html/ex63/hw63/app/Resources/views/base.html.twig");
    }
}
