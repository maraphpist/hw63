<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_88aa5590e1728b9aa9f696c69b5f470835caa9d563c4be92d673ae82c175976d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55adf6c45cf8909e4cd117ece93a2cead17af9071020b700dd8dadb1cd79c87f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55adf6c45cf8909e4cd117ece93a2cead17af9071020b700dd8dadb1cd79c87f->enter($__internal_55adf6c45cf8909e4cd117ece93a2cead17af9071020b700dd8dadb1cd79c87f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_2fb0083ae03f17db030203035a541c1f5571dab57a6ecdda3946569f4be490f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fb0083ae03f17db030203035a541c1f5571dab57a6ecdda3946569f4be490f4->enter($__internal_2fb0083ae03f17db030203035a541c1f5571dab57a6ecdda3946569f4be490f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_55adf6c45cf8909e4cd117ece93a2cead17af9071020b700dd8dadb1cd79c87f->leave($__internal_55adf6c45cf8909e4cd117ece93a2cead17af9071020b700dd8dadb1cd79c87f_prof);

        
        $__internal_2fb0083ae03f17db030203035a541c1f5571dab57a6ecdda3946569f4be490f4->leave($__internal_2fb0083ae03f17db030203035a541c1f5571dab57a6ecdda3946569f4be490f4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
