<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_77cb403ac9f5d693785a8bf26efa5d431e95dcc39820057b02a197d57f751ce0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6da372f3c50f5cb95f3c7b316a69313eeeec1ef5d7398b7eae839117877fc67e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6da372f3c50f5cb95f3c7b316a69313eeeec1ef5d7398b7eae839117877fc67e->enter($__internal_6da372f3c50f5cb95f3c7b316a69313eeeec1ef5d7398b7eae839117877fc67e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_3454bb9c732c4efad771c5d7b0aa950ed2dc24119313492a8a245dcb670e30cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3454bb9c732c4efad771c5d7b0aa950ed2dc24119313492a8a245dcb670e30cb->enter($__internal_3454bb9c732c4efad771c5d7b0aa950ed2dc24119313492a8a245dcb670e30cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_6da372f3c50f5cb95f3c7b316a69313eeeec1ef5d7398b7eae839117877fc67e->leave($__internal_6da372f3c50f5cb95f3c7b316a69313eeeec1ef5d7398b7eae839117877fc67e_prof);

        
        $__internal_3454bb9c732c4efad771c5d7b0aa950ed2dc24119313492a8a245dcb670e30cb->leave($__internal_3454bb9c732c4efad771c5d7b0aa950ed2dc24119313492a8a245dcb670e30cb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
