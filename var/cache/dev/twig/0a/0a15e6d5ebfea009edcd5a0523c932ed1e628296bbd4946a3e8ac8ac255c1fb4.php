<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_7bd36af570d7e8e3cd6332c5b810750aae9bc5356b7a1fc7226b578b8debba57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39f536d6eeac52a490c54f26d738e96a4b0edc443866a6f7e26efaae43c2f129 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39f536d6eeac52a490c54f26d738e96a4b0edc443866a6f7e26efaae43c2f129->enter($__internal_39f536d6eeac52a490c54f26d738e96a4b0edc443866a6f7e26efaae43c2f129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_eaed3f297eb225ddb3603474783274575e7d9cf1f1f5074bc10812bab16f9984 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eaed3f297eb225ddb3603474783274575e7d9cf1f1f5074bc10812bab16f9984->enter($__internal_eaed3f297eb225ddb3603474783274575e7d9cf1f1f5074bc10812bab16f9984_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_39f536d6eeac52a490c54f26d738e96a4b0edc443866a6f7e26efaae43c2f129->leave($__internal_39f536d6eeac52a490c54f26d738e96a4b0edc443866a6f7e26efaae43c2f129_prof);

        
        $__internal_eaed3f297eb225ddb3603474783274575e7d9cf1f1f5074bc10812bab16f9984->leave($__internal_eaed3f297eb225ddb3603474783274575e7d9cf1f1f5074bc10812bab16f9984_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
