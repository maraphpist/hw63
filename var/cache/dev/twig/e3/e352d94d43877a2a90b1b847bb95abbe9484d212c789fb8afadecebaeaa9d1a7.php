<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_9f4f286450227213bf58a0359df4d302b4fd04e289a400a5467910f0c3276bf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75598fecf30b7f9dcca88d8ce089275d3c8400349359604f9efd81a9234a3f2a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75598fecf30b7f9dcca88d8ce089275d3c8400349359604f9efd81a9234a3f2a->enter($__internal_75598fecf30b7f9dcca88d8ce089275d3c8400349359604f9efd81a9234a3f2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_8138244d46eab28bb71e175cd90b1decb032ebcc125490596333b53b8a8c5245 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8138244d46eab28bb71e175cd90b1decb032ebcc125490596333b53b8a8c5245->enter($__internal_8138244d46eab28bb71e175cd90b1decb032ebcc125490596333b53b8a8c5245_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_75598fecf30b7f9dcca88d8ce089275d3c8400349359604f9efd81a9234a3f2a->leave($__internal_75598fecf30b7f9dcca88d8ce089275d3c8400349359604f9efd81a9234a3f2a_prof);

        
        $__internal_8138244d46eab28bb71e175cd90b1decb032ebcc125490596333b53b8a8c5245->leave($__internal_8138244d46eab28bb71e175cd90b1decb032ebcc125490596333b53b8a8c5245_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a218821b991efe4aa2a5ead7f6021298d24801065f83360c87f9f9d15a4b8fb9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a218821b991efe4aa2a5ead7f6021298d24801065f83360c87f9f9d15a4b8fb9->enter($__internal_a218821b991efe4aa2a5ead7f6021298d24801065f83360c87f9f9d15a4b8fb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d8f5db99d96cf3be13188b5a71d8b52051d1d6727d8377ff394da1f7f246ad8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8f5db99d96cf3be13188b5a71d8b52051d1d6727d8377ff394da1f7f246ad8b->enter($__internal_d8f5db99d96cf3be13188b5a71d8b52051d1d6727d8377ff394da1f7f246ad8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_d8f5db99d96cf3be13188b5a71d8b52051d1d6727d8377ff394da1f7f246ad8b->leave($__internal_d8f5db99d96cf3be13188b5a71d8b52051d1d6727d8377ff394da1f7f246ad8b_prof);

        
        $__internal_a218821b991efe4aa2a5ead7f6021298d24801065f83360c87f9f9d15a4b8fb9->leave($__internal_a218821b991efe4aa2a5ead7f6021298d24801065f83360c87f9f9d15a4b8fb9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
