<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_bcee72b5a49fa11ebb864b9c54977b53673b38d5ff421148bb4188ba0eead1f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77c092064ee771a262563c731f0647b1576b7f639a4b7f7e0ef0cf3ac81dccb7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_77c092064ee771a262563c731f0647b1576b7f639a4b7f7e0ef0cf3ac81dccb7->enter($__internal_77c092064ee771a262563c731f0647b1576b7f639a4b7f7e0ef0cf3ac81dccb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_9a891c9caea1043b0c2d0edd346690f5b5f6fbab630f9af5c7dfd89768dae8f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a891c9caea1043b0c2d0edd346690f5b5f6fbab630f9af5c7dfd89768dae8f5->enter($__internal_9a891c9caea1043b0c2d0edd346690f5b5f6fbab630f9af5c7dfd89768dae8f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_77c092064ee771a262563c731f0647b1576b7f639a4b7f7e0ef0cf3ac81dccb7->leave($__internal_77c092064ee771a262563c731f0647b1576b7f639a4b7f7e0ef0cf3ac81dccb7_prof);

        
        $__internal_9a891c9caea1043b0c2d0edd346690f5b5f6fbab630f9af5c7dfd89768dae8f5->leave($__internal_9a891c9caea1043b0c2d0edd346690f5b5f6fbab630f9af5c7dfd89768dae8f5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
