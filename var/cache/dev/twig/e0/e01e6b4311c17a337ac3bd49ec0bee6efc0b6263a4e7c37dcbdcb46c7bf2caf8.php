<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_c5754d36886d24c4f610130a570c8fe34f3f8f69e055e2f74e9a6a27c34170a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0cd494148b20e33f077cd5679885683381511fbf7e8121982cf99744d8747f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0cd494148b20e33f077cd5679885683381511fbf7e8121982cf99744d8747f7->enter($__internal_e0cd494148b20e33f077cd5679885683381511fbf7e8121982cf99744d8747f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_d022566f599bf85c823454a5f0db63b6f3587054a09afc6d1e90a137c7db14e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d022566f599bf85c823454a5f0db63b6f3587054a09afc6d1e90a137c7db14e7->enter($__internal_d022566f599bf85c823454a5f0db63b6f3587054a09afc6d1e90a137c7db14e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_e0cd494148b20e33f077cd5679885683381511fbf7e8121982cf99744d8747f7->leave($__internal_e0cd494148b20e33f077cd5679885683381511fbf7e8121982cf99744d8747f7_prof);

        
        $__internal_d022566f599bf85c823454a5f0db63b6f3587054a09afc6d1e90a137c7db14e7->leave($__internal_d022566f599bf85c823454a5f0db63b6f3587054a09afc6d1e90a137c7db14e7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
