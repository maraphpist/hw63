<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_6411630e49d7556c6b402a950d82b722ad3ab8c454cda3acc41d70aeb16697a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e8480218cf792c37641e9682bd73e8230d449cc5c96ad447d0bb012dc3f70440 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e8480218cf792c37641e9682bd73e8230d449cc5c96ad447d0bb012dc3f70440->enter($__internal_e8480218cf792c37641e9682bd73e8230d449cc5c96ad447d0bb012dc3f70440_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_fee4cabce58728e4674d3d00e6722005c64a3847f9d9e8d93271c000ba252ddc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fee4cabce58728e4674d3d00e6722005c64a3847f9d9e8d93271c000ba252ddc->enter($__internal_fee4cabce58728e4674d3d00e6722005c64a3847f9d9e8d93271c000ba252ddc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_e8480218cf792c37641e9682bd73e8230d449cc5c96ad447d0bb012dc3f70440->leave($__internal_e8480218cf792c37641e9682bd73e8230d449cc5c96ad447d0bb012dc3f70440_prof);

        
        $__internal_fee4cabce58728e4674d3d00e6722005c64a3847f9d9e8d93271c000ba252ddc->leave($__internal_fee4cabce58728e4674d3d00e6722005c64a3847f9d9e8d93271c000ba252ddc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
