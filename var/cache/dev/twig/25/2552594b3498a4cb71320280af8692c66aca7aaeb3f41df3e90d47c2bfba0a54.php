<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_5822d96dc649f46e30dff249a3a71de6a19ae062cb782a290b55e829dac1467e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ab636a6cc026eceebcc85face525034bdc4618eae6685d90ef5de3b83b31c72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ab636a6cc026eceebcc85face525034bdc4618eae6685d90ef5de3b83b31c72->enter($__internal_1ab636a6cc026eceebcc85face525034bdc4618eae6685d90ef5de3b83b31c72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_15dea44d898f0df0f89ed4cab14a9587653aa9b82444fc6d2bf8944526748078 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15dea44d898f0df0f89ed4cab14a9587653aa9b82444fc6d2bf8944526748078->enter($__internal_15dea44d898f0df0f89ed4cab14a9587653aa9b82444fc6d2bf8944526748078_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_1ab636a6cc026eceebcc85face525034bdc4618eae6685d90ef5de3b83b31c72->leave($__internal_1ab636a6cc026eceebcc85face525034bdc4618eae6685d90ef5de3b83b31c72_prof);

        
        $__internal_15dea44d898f0df0f89ed4cab14a9587653aa9b82444fc6d2bf8944526748078->leave($__internal_15dea44d898f0df0f89ed4cab14a9587653aa9b82444fc6d2bf8944526748078_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ea8def1ba76522912a78fc60950e325aec367c4a711e879ac49da4a435095087 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea8def1ba76522912a78fc60950e325aec367c4a711e879ac49da4a435095087->enter($__internal_ea8def1ba76522912a78fc60950e325aec367c4a711e879ac49da4a435095087_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_83e9ad1144f663517a96095c19bc890d0ccd6bbcbd3878f37e126590e0d4ee76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83e9ad1144f663517a96095c19bc890d0ccd6bbcbd3878f37e126590e0d4ee76->enter($__internal_83e9ad1144f663517a96095c19bc890d0ccd6bbcbd3878f37e126590e0d4ee76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_83e9ad1144f663517a96095c19bc890d0ccd6bbcbd3878f37e126590e0d4ee76->leave($__internal_83e9ad1144f663517a96095c19bc890d0ccd6bbcbd3878f37e126590e0d4ee76_prof);

        
        $__internal_ea8def1ba76522912a78fc60950e325aec367c4a711e879ac49da4a435095087->leave($__internal_ea8def1ba76522912a78fc60950e325aec367c4a711e879ac49da4a435095087_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_664614cec920efc55babc31ca19cd6fbcf63ffc12253321e5da9bfb122ad28cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_664614cec920efc55babc31ca19cd6fbcf63ffc12253321e5da9bfb122ad28cf->enter($__internal_664614cec920efc55babc31ca19cd6fbcf63ffc12253321e5da9bfb122ad28cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_bef28afcc1fad32d19fe36e3ca4c34ffb53f76ac982da152369092abcf4b5448 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bef28afcc1fad32d19fe36e3ca4c34ffb53f76ac982da152369092abcf4b5448->enter($__internal_bef28afcc1fad32d19fe36e3ca4c34ffb53f76ac982da152369092abcf4b5448_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_bef28afcc1fad32d19fe36e3ca4c34ffb53f76ac982da152369092abcf4b5448->leave($__internal_bef28afcc1fad32d19fe36e3ca4c34ffb53f76ac982da152369092abcf4b5448_prof);

        
        $__internal_664614cec920efc55babc31ca19cd6fbcf63ffc12253321e5da9bfb122ad28cf->leave($__internal_664614cec920efc55babc31ca19cd6fbcf63ffc12253321e5da9bfb122ad28cf_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_8d850602ee1d5a7b3ba943c2b4641b9f9176ac9644336b1be855a81d195e409b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d850602ee1d5a7b3ba943c2b4641b9f9176ac9644336b1be855a81d195e409b->enter($__internal_8d850602ee1d5a7b3ba943c2b4641b9f9176ac9644336b1be855a81d195e409b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_01375a71a19a025649e14ca9a4091a2b2f6c80a9a47075b65955b515a669caf9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01375a71a19a025649e14ca9a4091a2b2f6c80a9a47075b65955b515a669caf9->enter($__internal_01375a71a19a025649e14ca9a4091a2b2f6c80a9a47075b65955b515a669caf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_01375a71a19a025649e14ca9a4091a2b2f6c80a9a47075b65955b515a669caf9->leave($__internal_01375a71a19a025649e14ca9a4091a2b2f6c80a9a47075b65955b515a669caf9_prof);

        
        $__internal_8d850602ee1d5a7b3ba943c2b4641b9f9176ac9644336b1be855a81d195e409b->leave($__internal_8d850602ee1d5a7b3ba943c2b4641b9f9176ac9644336b1be855a81d195e409b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
