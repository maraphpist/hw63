<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_c7a3586c769d8ac8fbeb9126643e286decb1acada38f88a003d0ee18bd3bacc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33aa6d6b6961ccf2f0e5272ad4b4505ced438233c6db5e3ddb850da80e155ded = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33aa6d6b6961ccf2f0e5272ad4b4505ced438233c6db5e3ddb850da80e155ded->enter($__internal_33aa6d6b6961ccf2f0e5272ad4b4505ced438233c6db5e3ddb850da80e155ded_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_ce8a2d0a0ad0d5d96fd3818a2ef92d77ac246078357814814b5dd8751c9243ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce8a2d0a0ad0d5d96fd3818a2ef92d77ac246078357814814b5dd8751c9243ad->enter($__internal_ce8a2d0a0ad0d5d96fd3818a2ef92d77ac246078357814814b5dd8751c9243ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_33aa6d6b6961ccf2f0e5272ad4b4505ced438233c6db5e3ddb850da80e155ded->leave($__internal_33aa6d6b6961ccf2f0e5272ad4b4505ced438233c6db5e3ddb850da80e155ded_prof);

        
        $__internal_ce8a2d0a0ad0d5d96fd3818a2ef92d77ac246078357814814b5dd8751c9243ad->leave($__internal_ce8a2d0a0ad0d5d96fd3818a2ef92d77ac246078357814814b5dd8751c9243ad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
