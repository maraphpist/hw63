<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_11c34f90e1aa70931d1156a33513002793bc32544b5198745360de3ad7100084 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da452ce62c560c81d59fb177d4215f5af194aedd5d852688a5e96c0c9b59cb7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da452ce62c560c81d59fb177d4215f5af194aedd5d852688a5e96c0c9b59cb7e->enter($__internal_da452ce62c560c81d59fb177d4215f5af194aedd5d852688a5e96c0c9b59cb7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_10d68f088cc4c276dcb1727a402a5dff2024efb8d3cf0db5530d4c77acc6d536 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10d68f088cc4c276dcb1727a402a5dff2024efb8d3cf0db5530d4c77acc6d536->enter($__internal_10d68f088cc4c276dcb1727a402a5dff2024efb8d3cf0db5530d4c77acc6d536_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_da452ce62c560c81d59fb177d4215f5af194aedd5d852688a5e96c0c9b59cb7e->leave($__internal_da452ce62c560c81d59fb177d4215f5af194aedd5d852688a5e96c0c9b59cb7e_prof);

        
        $__internal_10d68f088cc4c276dcb1727a402a5dff2024efb8d3cf0db5530d4c77acc6d536->leave($__internal_10d68f088cc4c276dcb1727a402a5dff2024efb8d3cf0db5530d4c77acc6d536_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
