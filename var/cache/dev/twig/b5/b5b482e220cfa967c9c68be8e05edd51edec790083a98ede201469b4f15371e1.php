<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_2d17f17f30d4f3ea6a640d73891facb6213a880f8e7e5d75994f5833324c3af7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22707d7810b224e2cca24f5d39a29a1eb539b0e870e49416303af6938c00ec95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22707d7810b224e2cca24f5d39a29a1eb539b0e870e49416303af6938c00ec95->enter($__internal_22707d7810b224e2cca24f5d39a29a1eb539b0e870e49416303af6938c00ec95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_7545a1aa1405a002f91d834b9982f40387d56452fa97cee652aa0ea981fa2fbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7545a1aa1405a002f91d834b9982f40387d56452fa97cee652aa0ea981fa2fbd->enter($__internal_7545a1aa1405a002f91d834b9982f40387d56452fa97cee652aa0ea981fa2fbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_22707d7810b224e2cca24f5d39a29a1eb539b0e870e49416303af6938c00ec95->leave($__internal_22707d7810b224e2cca24f5d39a29a1eb539b0e870e49416303af6938c00ec95_prof);

        
        $__internal_7545a1aa1405a002f91d834b9982f40387d56452fa97cee652aa0ea981fa2fbd->leave($__internal_7545a1aa1405a002f91d834b9982f40387d56452fa97cee652aa0ea981fa2fbd_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_02397185495098d448035e7d02a4bc77f65e0accce204b4c9d25cbf16928ef1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02397185495098d448035e7d02a4bc77f65e0accce204b4c9d25cbf16928ef1a->enter($__internal_02397185495098d448035e7d02a4bc77f65e0accce204b4c9d25cbf16928ef1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_48fd3639985c9bb423ae0ac50568688076a03152a3ba168edc229ba6a9bce78f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_48fd3639985c9bb423ae0ac50568688076a03152a3ba168edc229ba6a9bce78f->enter($__internal_48fd3639985c9bb423ae0ac50568688076a03152a3ba168edc229ba6a9bce78f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_48fd3639985c9bb423ae0ac50568688076a03152a3ba168edc229ba6a9bce78f->leave($__internal_48fd3639985c9bb423ae0ac50568688076a03152a3ba168edc229ba6a9bce78f_prof);

        
        $__internal_02397185495098d448035e7d02a4bc77f65e0accce204b4c9d25cbf16928ef1a->leave($__internal_02397185495098d448035e7d02a4bc77f65e0accce204b4c9d25cbf16928ef1a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
