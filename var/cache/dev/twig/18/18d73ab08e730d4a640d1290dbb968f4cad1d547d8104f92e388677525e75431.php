<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_d1f22b4d567290d22c377e28534b534b6be4ca8229b3cd7918f74ffa5c91100c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dec3e65eb7c7e319c1042dc31f44e847cd6be10172ed72128b5b68e40536392c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dec3e65eb7c7e319c1042dc31f44e847cd6be10172ed72128b5b68e40536392c->enter($__internal_dec3e65eb7c7e319c1042dc31f44e847cd6be10172ed72128b5b68e40536392c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_36b50529a3be49c5e4d56a115a9c4e9ff34c19b6d3480066084eb84d9d8c7cf4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36b50529a3be49c5e4d56a115a9c4e9ff34c19b6d3480066084eb84d9d8c7cf4->enter($__internal_36b50529a3be49c5e4d56a115a9c4e9ff34c19b6d3480066084eb84d9d8c7cf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_dec3e65eb7c7e319c1042dc31f44e847cd6be10172ed72128b5b68e40536392c->leave($__internal_dec3e65eb7c7e319c1042dc31f44e847cd6be10172ed72128b5b68e40536392c_prof);

        
        $__internal_36b50529a3be49c5e4d56a115a9c4e9ff34c19b6d3480066084eb84d9d8c7cf4->leave($__internal_36b50529a3be49c5e4d56a115a9c4e9ff34c19b6d3480066084eb84d9d8c7cf4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
