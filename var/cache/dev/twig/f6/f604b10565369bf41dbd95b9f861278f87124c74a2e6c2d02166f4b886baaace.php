<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_94f2eb4198013dc295c045882ffcee0c845e89a9371ac29f4ad50235d639f4ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db1b34696793d44b37fe8aeb5d8bfaa6b13cb5b7126e1d096cfe7142bd44f862 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db1b34696793d44b37fe8aeb5d8bfaa6b13cb5b7126e1d096cfe7142bd44f862->enter($__internal_db1b34696793d44b37fe8aeb5d8bfaa6b13cb5b7126e1d096cfe7142bd44f862_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_f3bcfabb3e01c995ad7f187f7fa907b2e7a6b30b0e57cb6454f0ceb6f53fd988 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3bcfabb3e01c995ad7f187f7fa907b2e7a6b30b0e57cb6454f0ceb6f53fd988->enter($__internal_f3bcfabb3e01c995ad7f187f7fa907b2e7a6b30b0e57cb6454f0ceb6f53fd988_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_db1b34696793d44b37fe8aeb5d8bfaa6b13cb5b7126e1d096cfe7142bd44f862->leave($__internal_db1b34696793d44b37fe8aeb5d8bfaa6b13cb5b7126e1d096cfe7142bd44f862_prof);

        
        $__internal_f3bcfabb3e01c995ad7f187f7fa907b2e7a6b30b0e57cb6454f0ceb6f53fd988->leave($__internal_f3bcfabb3e01c995ad7f187f7fa907b2e7a6b30b0e57cb6454f0ceb6f53fd988_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
