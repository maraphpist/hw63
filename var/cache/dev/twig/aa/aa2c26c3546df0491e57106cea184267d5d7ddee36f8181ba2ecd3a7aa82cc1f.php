<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_44e449d7e286c49bde1eb2722d0db18bc1a44e15941931a92381e123f94f0614 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d5eb6071d6ce56740040a6e228c0befbba7cbb6bbf3edd1c79ec207cb060d508 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5eb6071d6ce56740040a6e228c0befbba7cbb6bbf3edd1c79ec207cb060d508->enter($__internal_d5eb6071d6ce56740040a6e228c0befbba7cbb6bbf3edd1c79ec207cb060d508_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_1453659c5237dc6ee16468b9e23a6de99cd670978c9b83618b3316f17de69e64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1453659c5237dc6ee16468b9e23a6de99cd670978c9b83618b3316f17de69e64->enter($__internal_1453659c5237dc6ee16468b9e23a6de99cd670978c9b83618b3316f17de69e64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_d5eb6071d6ce56740040a6e228c0befbba7cbb6bbf3edd1c79ec207cb060d508->leave($__internal_d5eb6071d6ce56740040a6e228c0befbba7cbb6bbf3edd1c79ec207cb060d508_prof);

        
        $__internal_1453659c5237dc6ee16468b9e23a6de99cd670978c9b83618b3316f17de69e64->leave($__internal_1453659c5237dc6ee16468b9e23a6de99cd670978c9b83618b3316f17de69e64_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
