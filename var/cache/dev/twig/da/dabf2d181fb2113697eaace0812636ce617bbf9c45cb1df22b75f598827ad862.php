<?php

/* :default:index.html.twig */
class __TwigTemplate_e7dd53a1d59c18ca9afe194e1bc787facc3052bd1f139ee8df3eac6ed5269f37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":default:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6b813b870ec1ba01ba1fc294a92c0ea73607be585d741abeacbf0b618ec1433 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6b813b870ec1ba01ba1fc294a92c0ea73607be585d741abeacbf0b618ec1433->enter($__internal_c6b813b870ec1ba01ba1fc294a92c0ea73607be585d741abeacbf0b618ec1433_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:index.html.twig"));

        $__internal_f412139d009b73c9020763e9a9e5ec4b76d205a669f49f6c83b75e56e2ed14d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f412139d009b73c9020763e9a9e5ec4b76d205a669f49f6c83b75e56e2ed14d1->enter($__internal_f412139d009b73c9020763e9a9e5ec4b76d205a669f49f6c83b75e56e2ed14d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c6b813b870ec1ba01ba1fc294a92c0ea73607be585d741abeacbf0b618ec1433->leave($__internal_c6b813b870ec1ba01ba1fc294a92c0ea73607be585d741abeacbf0b618ec1433_prof);

        
        $__internal_f412139d009b73c9020763e9a9e5ec4b76d205a669f49f6c83b75e56e2ed14d1->leave($__internal_f412139d009b73c9020763e9a9e5ec4b76d205a669f49f6c83b75e56e2ed14d1_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d12b24cd8693db305e93f8c4ff162446fef743ed15ff0375dff869b769c79b88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d12b24cd8693db305e93f8c4ff162446fef743ed15ff0375dff869b769c79b88->enter($__internal_d12b24cd8693db305e93f8c4ff162446fef743ed15ff0375dff869b769c79b88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_13e4d245a5354b3523701100d835659620146af5ad4326494afa65b33265d163 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13e4d245a5354b3523701100d835659620146af5ad4326494afa65b33265d163->enter($__internal_13e4d245a5354b3523701100d835659620146af5ad4326494afa65b33265d163_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 5
            echo "        <tr>
            <td>
                <a href=\"";
            // line 7
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_default_detail", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["category"], "translate", array(0 => "ru"), "method"), "name", array()), "html", null, true);
            echo "</a><br>
            </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_13e4d245a5354b3523701100d835659620146af5ad4326494afa65b33265d163->leave($__internal_13e4d245a5354b3523701100d835659620146af5ad4326494afa65b33265d163_prof);

        
        $__internal_d12b24cd8693db305e93f8c4ff162446fef743ed15ff0375dff869b769c79b88->leave($__internal_d12b24cd8693db305e93f8c4ff162446fef743ed15ff0375dff869b769c79b88_prof);

    }

    public function getTemplateName()
    {
        return ":default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 13,  75 => 12,  70 => 11,  58 => 7,  54 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    {% for category in categories %}
        <tr>
            <td>
                <a href=\"{{ path('app_default_detail', {'id': category.id }) }}\">{{ category.translate('ru').name }}</a><br>
            </td>
        </tr>
    {% endfor %}
    {{ form_start(form) }}
    {{ form_widget(form) }}
    {{ form_end(form) }}
{% endblock %}
", ":default:index.html.twig", "/var/www/html/ex63/hw63/app/Resources/views/default/index.html.twig");
    }
}
