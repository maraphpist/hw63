<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_9c4f657469d396758145b0b399b9355e90943c3e96346630fc1eab2a16180b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_329b9d3717ce9cbd2544d5b6d5a1d61a97109079ee009be31de96fce3f6b6f02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_329b9d3717ce9cbd2544d5b6d5a1d61a97109079ee009be31de96fce3f6b6f02->enter($__internal_329b9d3717ce9cbd2544d5b6d5a1d61a97109079ee009be31de96fce3f6b6f02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_9ea1ccbd7dfac061d38bfb498d606d94f1ed844bb9b6c5a2d34e3ceaf70868a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ea1ccbd7dfac061d38bfb498d606d94f1ed844bb9b6c5a2d34e3ceaf70868a1->enter($__internal_9ea1ccbd7dfac061d38bfb498d606d94f1ed844bb9b6c5a2d34e3ceaf70868a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_329b9d3717ce9cbd2544d5b6d5a1d61a97109079ee009be31de96fce3f6b6f02->leave($__internal_329b9d3717ce9cbd2544d5b6d5a1d61a97109079ee009be31de96fce3f6b6f02_prof);

        
        $__internal_9ea1ccbd7dfac061d38bfb498d606d94f1ed844bb9b6c5a2d34e3ceaf70868a1->leave($__internal_9ea1ccbd7dfac061d38bfb498d606d94f1ed844bb9b6c5a2d34e3ceaf70868a1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
