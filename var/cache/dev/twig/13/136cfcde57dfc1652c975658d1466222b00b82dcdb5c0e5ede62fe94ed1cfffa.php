<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_8bb5a475bcef4e0db251738308df8e47dbc48a1cd1c7120cdf9a8487afd6b071 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5c868cdf96edf0e3d3d25cb29751e8e1e3028617f1075eb08c3e54b4b3dd0f10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c868cdf96edf0e3d3d25cb29751e8e1e3028617f1075eb08c3e54b4b3dd0f10->enter($__internal_5c868cdf96edf0e3d3d25cb29751e8e1e3028617f1075eb08c3e54b4b3dd0f10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_7d87e6287a6aad958afb1502a9a72e593e418db88bcc3ee2a4ae4ee3bce450b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d87e6287a6aad958afb1502a9a72e593e418db88bcc3ee2a4ae4ee3bce450b7->enter($__internal_7d87e6287a6aad958afb1502a9a72e593e418db88bcc3ee2a4ae4ee3bce450b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_5c868cdf96edf0e3d3d25cb29751e8e1e3028617f1075eb08c3e54b4b3dd0f10->leave($__internal_5c868cdf96edf0e3d3d25cb29751e8e1e3028617f1075eb08c3e54b4b3dd0f10_prof);

        
        $__internal_7d87e6287a6aad958afb1502a9a72e593e418db88bcc3ee2a4ae4ee3bce450b7->leave($__internal_7d87e6287a6aad958afb1502a9a72e593e418db88bcc3ee2a4ae4ee3bce450b7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
