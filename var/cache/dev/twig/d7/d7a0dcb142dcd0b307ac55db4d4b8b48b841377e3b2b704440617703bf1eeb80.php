<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_ba4eeaddd5d43926e9828b0f443f7b3eb6428665e77b8be38e5fb4fe681b7168 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35c4282e3daddbfc57d6b0fa73266b7086e9fbc9f6b53ee1946ce581321435ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35c4282e3daddbfc57d6b0fa73266b7086e9fbc9f6b53ee1946ce581321435ab->enter($__internal_35c4282e3daddbfc57d6b0fa73266b7086e9fbc9f6b53ee1946ce581321435ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_59ecf1dc5b9e020540a73f661ba564ac0927c4f715d14c1383baf97f329df9f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59ecf1dc5b9e020540a73f661ba564ac0927c4f715d14c1383baf97f329df9f0->enter($__internal_59ecf1dc5b9e020540a73f661ba564ac0927c4f715d14c1383baf97f329df9f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_35c4282e3daddbfc57d6b0fa73266b7086e9fbc9f6b53ee1946ce581321435ab->leave($__internal_35c4282e3daddbfc57d6b0fa73266b7086e9fbc9f6b53ee1946ce581321435ab_prof);

        
        $__internal_59ecf1dc5b9e020540a73f661ba564ac0927c4f715d14c1383baf97f329df9f0->leave($__internal_59ecf1dc5b9e020540a73f661ba564ac0927c4f715d14c1383baf97f329df9f0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
