<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_17ba7283a10a58f663a8c9cfa9a16567887e0ac21fd5fa929fb362fd4db8effa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7957ed00225ff996671f15278b5d5b878ea42fceaca7b4cd138e10405ae74ab2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7957ed00225ff996671f15278b5d5b878ea42fceaca7b4cd138e10405ae74ab2->enter($__internal_7957ed00225ff996671f15278b5d5b878ea42fceaca7b4cd138e10405ae74ab2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_559de8394163071e7968e7b8f64d2e2c09d06895b993df62227a8cd87e92504c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_559de8394163071e7968e7b8f64d2e2c09d06895b993df62227a8cd87e92504c->enter($__internal_559de8394163071e7968e7b8f64d2e2c09d06895b993df62227a8cd87e92504c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_7957ed00225ff996671f15278b5d5b878ea42fceaca7b4cd138e10405ae74ab2->leave($__internal_7957ed00225ff996671f15278b5d5b878ea42fceaca7b4cd138e10405ae74ab2_prof);

        
        $__internal_559de8394163071e7968e7b8f64d2e2c09d06895b993df62227a8cd87e92504c->leave($__internal_559de8394163071e7968e7b8f64d2e2c09d06895b993df62227a8cd87e92504c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
