<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_04b7e365edc6d0270359b108e26ab84aed7e2c8714c81d85055c1b56c5fbfa66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa51250a1ff4310a8be6a96be8a810ca6a4ca5c047688862f688d4b548b64352 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa51250a1ff4310a8be6a96be8a810ca6a4ca5c047688862f688d4b548b64352->enter($__internal_fa51250a1ff4310a8be6a96be8a810ca6a4ca5c047688862f688d4b548b64352_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_31c2223b2721440bcefdc6e8a808da1b4586f607e47b536ee2a315df209c0322 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31c2223b2721440bcefdc6e8a808da1b4586f607e47b536ee2a315df209c0322->enter($__internal_31c2223b2721440bcefdc6e8a808da1b4586f607e47b536ee2a315df209c0322_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa51250a1ff4310a8be6a96be8a810ca6a4ca5c047688862f688d4b548b64352->leave($__internal_fa51250a1ff4310a8be6a96be8a810ca6a4ca5c047688862f688d4b548b64352_prof);

        
        $__internal_31c2223b2721440bcefdc6e8a808da1b4586f607e47b536ee2a315df209c0322->leave($__internal_31c2223b2721440bcefdc6e8a808da1b4586f607e47b536ee2a315df209c0322_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5d4f6030b964756894247cbd6844f2fbb1dbb167aa33ec321eb02839bd1498e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d4f6030b964756894247cbd6844f2fbb1dbb167aa33ec321eb02839bd1498e3->enter($__internal_5d4f6030b964756894247cbd6844f2fbb1dbb167aa33ec321eb02839bd1498e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ad514514349f9f6f727ee0b2cadc39e5bdda4d321a0ecd78b63498fe998059f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad514514349f9f6f727ee0b2cadc39e5bdda4d321a0ecd78b63498fe998059f9->enter($__internal_ad514514349f9f6f727ee0b2cadc39e5bdda4d321a0ecd78b63498fe998059f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_ad514514349f9f6f727ee0b2cadc39e5bdda4d321a0ecd78b63498fe998059f9->leave($__internal_ad514514349f9f6f727ee0b2cadc39e5bdda4d321a0ecd78b63498fe998059f9_prof);

        
        $__internal_5d4f6030b964756894247cbd6844f2fbb1dbb167aa33ec321eb02839bd1498e3->leave($__internal_5d4f6030b964756894247cbd6844f2fbb1dbb167aa33ec321eb02839bd1498e3_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_622be6de3cc3d4b1f22c5422776ac98c499d7b7d0c71e77bf0049f62522523a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_622be6de3cc3d4b1f22c5422776ac98c499d7b7d0c71e77bf0049f62522523a0->enter($__internal_622be6de3cc3d4b1f22c5422776ac98c499d7b7d0c71e77bf0049f62522523a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b74be329ca822698fc177d2ec43b67bb7cd25af5e7a81560b45553a0f8c0d8a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b74be329ca822698fc177d2ec43b67bb7cd25af5e7a81560b45553a0f8c0d8a7->enter($__internal_b74be329ca822698fc177d2ec43b67bb7cd25af5e7a81560b45553a0f8c0d8a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_b74be329ca822698fc177d2ec43b67bb7cd25af5e7a81560b45553a0f8c0d8a7->leave($__internal_b74be329ca822698fc177d2ec43b67bb7cd25af5e7a81560b45553a0f8c0d8a7_prof);

        
        $__internal_622be6de3cc3d4b1f22c5422776ac98c499d7b7d0c71e77bf0049f62522523a0->leave($__internal_622be6de3cc3d4b1f22c5422776ac98c499d7b7d0c71e77bf0049f62522523a0_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
