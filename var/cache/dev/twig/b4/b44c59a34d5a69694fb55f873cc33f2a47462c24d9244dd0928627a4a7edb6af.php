<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_10f4786686d89a97183133610a4e6e9b6adc51207bfa05e3c6aa1b1d738a1018 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_203afda3e9fdab79e881ae33e0402f652576be6162c0796a08ba65ea568600f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_203afda3e9fdab79e881ae33e0402f652576be6162c0796a08ba65ea568600f4->enter($__internal_203afda3e9fdab79e881ae33e0402f652576be6162c0796a08ba65ea568600f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_a3adc81eddf2eca76a7208b908365ff523adda85aae131274d1c7c324fb2da9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3adc81eddf2eca76a7208b908365ff523adda85aae131274d1c7c324fb2da9d->enter($__internal_a3adc81eddf2eca76a7208b908365ff523adda85aae131274d1c7c324fb2da9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_203afda3e9fdab79e881ae33e0402f652576be6162c0796a08ba65ea568600f4->leave($__internal_203afda3e9fdab79e881ae33e0402f652576be6162c0796a08ba65ea568600f4_prof);

        
        $__internal_a3adc81eddf2eca76a7208b908365ff523adda85aae131274d1c7c324fb2da9d->leave($__internal_a3adc81eddf2eca76a7208b908365ff523adda85aae131274d1c7c324fb2da9d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
