<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_ba0aca08246e24f2528322e232d4de5528dafa300cad831a16f152ed5a73d85b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd5ac01ec8e54f0f3c157ccd8ce3f96f496fabb0fddc2561ba79e113b761d2b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dd5ac01ec8e54f0f3c157ccd8ce3f96f496fabb0fddc2561ba79e113b761d2b1->enter($__internal_dd5ac01ec8e54f0f3c157ccd8ce3f96f496fabb0fddc2561ba79e113b761d2b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_2db32b6b00521ba747bb48c5267428094cfb6c889171be82754edf3c69a8ed89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2db32b6b00521ba747bb48c5267428094cfb6c889171be82754edf3c69a8ed89->enter($__internal_2db32b6b00521ba747bb48c5267428094cfb6c889171be82754edf3c69a8ed89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dd5ac01ec8e54f0f3c157ccd8ce3f96f496fabb0fddc2561ba79e113b761d2b1->leave($__internal_dd5ac01ec8e54f0f3c157ccd8ce3f96f496fabb0fddc2561ba79e113b761d2b1_prof);

        
        $__internal_2db32b6b00521ba747bb48c5267428094cfb6c889171be82754edf3c69a8ed89->leave($__internal_2db32b6b00521ba747bb48c5267428094cfb6c889171be82754edf3c69a8ed89_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d4e13842d0a8a478355e4ed71261dbb58f22d0b04fcd28968c4469258c4ae626 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d4e13842d0a8a478355e4ed71261dbb58f22d0b04fcd28968c4469258c4ae626->enter($__internal_d4e13842d0a8a478355e4ed71261dbb58f22d0b04fcd28968c4469258c4ae626_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_64b5e568b80b259b42ea62255bf02bbfdded3da907edf8add3881128904f2db7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64b5e568b80b259b42ea62255bf02bbfdded3da907edf8add3881128904f2db7->enter($__internal_64b5e568b80b259b42ea62255bf02bbfdded3da907edf8add3881128904f2db7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_64b5e568b80b259b42ea62255bf02bbfdded3da907edf8add3881128904f2db7->leave($__internal_64b5e568b80b259b42ea62255bf02bbfdded3da907edf8add3881128904f2db7_prof);

        
        $__internal_d4e13842d0a8a478355e4ed71261dbb58f22d0b04fcd28968c4469258c4ae626->leave($__internal_d4e13842d0a8a478355e4ed71261dbb58f22d0b04fcd28968c4469258c4ae626_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/var/www/html/ex63/hw63/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
