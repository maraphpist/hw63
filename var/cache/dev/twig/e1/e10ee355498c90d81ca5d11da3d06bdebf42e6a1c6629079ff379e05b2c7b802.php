<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_090b1f152a917b9f715044b30f08eed4dd30828b3a94637058ffbccb5cdae65b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3f736f0cd2faeba64f37aa5e71daaa0fb7968bc693ddb89556e6862934cc2b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3f736f0cd2faeba64f37aa5e71daaa0fb7968bc693ddb89556e6862934cc2b5->enter($__internal_a3f736f0cd2faeba64f37aa5e71daaa0fb7968bc693ddb89556e6862934cc2b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_fd0eaa4f47fd8c63914532c62b91e913f4112b085af2f5474fc924abfc7a90d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd0eaa4f47fd8c63914532c62b91e913f4112b085af2f5474fc924abfc7a90d7->enter($__internal_fd0eaa4f47fd8c63914532c62b91e913f4112b085af2f5474fc924abfc7a90d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_a3f736f0cd2faeba64f37aa5e71daaa0fb7968bc693ddb89556e6862934cc2b5->leave($__internal_a3f736f0cd2faeba64f37aa5e71daaa0fb7968bc693ddb89556e6862934cc2b5_prof);

        
        $__internal_fd0eaa4f47fd8c63914532c62b91e913f4112b085af2f5474fc924abfc7a90d7->leave($__internal_fd0eaa4f47fd8c63914532c62b91e913f4112b085af2f5474fc924abfc7a90d7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
