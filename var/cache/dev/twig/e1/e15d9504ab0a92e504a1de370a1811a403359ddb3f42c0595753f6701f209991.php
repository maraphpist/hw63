<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_5c78ddec2dd0213e300968bd3cecc18c8277369c7402f3d95247e69c4acc98c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c571f337b806fbbe95f667a44a03775a28568cb55cc40a641aadaad60d6a844c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c571f337b806fbbe95f667a44a03775a28568cb55cc40a641aadaad60d6a844c->enter($__internal_c571f337b806fbbe95f667a44a03775a28568cb55cc40a641aadaad60d6a844c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_f3a43357233af1155c6e5a6f40fd4bc3631c16586c5be0b73c1fc2ab60815f51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3a43357233af1155c6e5a6f40fd4bc3631c16586c5be0b73c1fc2ab60815f51->enter($__internal_f3a43357233af1155c6e5a6f40fd4bc3631c16586c5be0b73c1fc2ab60815f51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c571f337b806fbbe95f667a44a03775a28568cb55cc40a641aadaad60d6a844c->leave($__internal_c571f337b806fbbe95f667a44a03775a28568cb55cc40a641aadaad60d6a844c_prof);

        
        $__internal_f3a43357233af1155c6e5a6f40fd4bc3631c16586c5be0b73c1fc2ab60815f51->leave($__internal_f3a43357233af1155c6e5a6f40fd4bc3631c16586c5be0b73c1fc2ab60815f51_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_2225bf0159186213735e00143e275fd66f134913ee633008947a86c92649be8d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2225bf0159186213735e00143e275fd66f134913ee633008947a86c92649be8d->enter($__internal_2225bf0159186213735e00143e275fd66f134913ee633008947a86c92649be8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_00624d96559eb914558b0e94b592518bc7843a221d9477f3f774d7e9af403fba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00624d96559eb914558b0e94b592518bc7843a221d9477f3f774d7e9af403fba->enter($__internal_00624d96559eb914558b0e94b592518bc7843a221d9477f3f774d7e9af403fba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_00624d96559eb914558b0e94b592518bc7843a221d9477f3f774d7e9af403fba->leave($__internal_00624d96559eb914558b0e94b592518bc7843a221d9477f3f774d7e9af403fba_prof);

        
        $__internal_2225bf0159186213735e00143e275fd66f134913ee633008947a86c92649be8d->leave($__internal_2225bf0159186213735e00143e275fd66f134913ee633008947a86c92649be8d_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_1705f7a662d8280261760ac82131eaa27b8689f679bb9b015098899db49c0d0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1705f7a662d8280261760ac82131eaa27b8689f679bb9b015098899db49c0d0b->enter($__internal_1705f7a662d8280261760ac82131eaa27b8689f679bb9b015098899db49c0d0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c93ff339ac891e5abd48baa4a40d8232fe8ef02d3d1277da924aa3aaa91e7c97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c93ff339ac891e5abd48baa4a40d8232fe8ef02d3d1277da924aa3aaa91e7c97->enter($__internal_c93ff339ac891e5abd48baa4a40d8232fe8ef02d3d1277da924aa3aaa91e7c97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_c93ff339ac891e5abd48baa4a40d8232fe8ef02d3d1277da924aa3aaa91e7c97->leave($__internal_c93ff339ac891e5abd48baa4a40d8232fe8ef02d3d1277da924aa3aaa91e7c97_prof);

        
        $__internal_1705f7a662d8280261760ac82131eaa27b8689f679bb9b015098899db49c0d0b->leave($__internal_1705f7a662d8280261760ac82131eaa27b8689f679bb9b015098899db49c0d0b_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
