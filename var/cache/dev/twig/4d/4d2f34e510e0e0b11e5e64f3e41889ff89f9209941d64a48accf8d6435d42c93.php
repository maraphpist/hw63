<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_6df3dfb24239c54f6ce06832815f88ac08190800daead0570c872412557bb9f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b601630d34037b84441766561a8cb3fbb588d7380b3883da2c4603b4fc302df3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b601630d34037b84441766561a8cb3fbb588d7380b3883da2c4603b4fc302df3->enter($__internal_b601630d34037b84441766561a8cb3fbb588d7380b3883da2c4603b4fc302df3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_43027e4f35a9a07102b11a30467046df173824c8d10b47f74dd471cb3a0bd33e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43027e4f35a9a07102b11a30467046df173824c8d10b47f74dd471cb3a0bd33e->enter($__internal_43027e4f35a9a07102b11a30467046df173824c8d10b47f74dd471cb3a0bd33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_b601630d34037b84441766561a8cb3fbb588d7380b3883da2c4603b4fc302df3->leave($__internal_b601630d34037b84441766561a8cb3fbb588d7380b3883da2c4603b4fc302df3_prof);

        
        $__internal_43027e4f35a9a07102b11a30467046df173824c8d10b47f74dd471cb3a0bd33e->leave($__internal_43027e4f35a9a07102b11a30467046df173824c8d10b47f74dd471cb3a0bd33e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
