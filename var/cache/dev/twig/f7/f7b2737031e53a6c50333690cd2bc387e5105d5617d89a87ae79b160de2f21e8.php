<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_289d0ec649af2677564e75fc0a745ef9ecf3d4d5cc0ff2792a47adf19c2b495f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22f8c188fb0032b6973d77b83dead87a26b1eb6d07ba983ef58e20366ae5f913 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22f8c188fb0032b6973d77b83dead87a26b1eb6d07ba983ef58e20366ae5f913->enter($__internal_22f8c188fb0032b6973d77b83dead87a26b1eb6d07ba983ef58e20366ae5f913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_6f39d4b107f38fafa00a8c728f7bdaea9ff5b005ff94b3e4522b79f6129dcc76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f39d4b107f38fafa00a8c728f7bdaea9ff5b005ff94b3e4522b79f6129dcc76->enter($__internal_6f39d4b107f38fafa00a8c728f7bdaea9ff5b005ff94b3e4522b79f6129dcc76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_22f8c188fb0032b6973d77b83dead87a26b1eb6d07ba983ef58e20366ae5f913->leave($__internal_22f8c188fb0032b6973d77b83dead87a26b1eb6d07ba983ef58e20366ae5f913_prof);

        
        $__internal_6f39d4b107f38fafa00a8c728f7bdaea9ff5b005ff94b3e4522b79f6129dcc76->leave($__internal_6f39d4b107f38fafa00a8c728f7bdaea9ff5b005ff94b3e4522b79f6129dcc76_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
