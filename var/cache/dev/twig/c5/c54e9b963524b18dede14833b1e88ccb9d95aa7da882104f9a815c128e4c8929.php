<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_37e46c76d02729b1ef2fea2576c1b1aba23cda9eef4c1c20a78c979265363716 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21bd12060c10a0a37da85e55e4a299b97ccece87cc97650a02044ca21a8f42fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21bd12060c10a0a37da85e55e4a299b97ccece87cc97650a02044ca21a8f42fe->enter($__internal_21bd12060c10a0a37da85e55e4a299b97ccece87cc97650a02044ca21a8f42fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_98b4ab3fae3290a047104ed036b8401a3d916dc096cc604952508a1b343121aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98b4ab3fae3290a047104ed036b8401a3d916dc096cc604952508a1b343121aa->enter($__internal_98b4ab3fae3290a047104ed036b8401a3d916dc096cc604952508a1b343121aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_21bd12060c10a0a37da85e55e4a299b97ccece87cc97650a02044ca21a8f42fe->leave($__internal_21bd12060c10a0a37da85e55e4a299b97ccece87cc97650a02044ca21a8f42fe_prof);

        
        $__internal_98b4ab3fae3290a047104ed036b8401a3d916dc096cc604952508a1b343121aa->leave($__internal_98b4ab3fae3290a047104ed036b8401a3d916dc096cc604952508a1b343121aa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/var/www/html/ex63/hw63/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
